{
  description = "SFA-Atlas flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.11;
  };
  outputs = { self, nixpkgs }:
    let
      project = "rooseli";
      system = "x86_64-linux";
      overlays = [
        (self: super: {
          haskellPackages = super.haskellPackages.override {
            overrides = haskellSelf: haskellSuper: {
              geos = self.haskell.lib.dontCheck (self.haskell.lib.unmarkBroken super.haskellPackages.geos);
              # jose-jwt = self.haskell.lib.unmarkBroken super.haskellPackages.jose-jwt;
            };
          };
        })
        (self: super: {
          rooseli = (self.haskellPackages.callCabal2nix "rooseli" ./server {});
        })
        (self: super: {
          rooseliExec = self.haskell.lib.justStaticExecutables super.rooseli;
        })
      ];
      pkgs = import nixpkgs {
        inherit system;
        overlays = overlays;
      };
      rooseliDbName = "rooseli"; # the ultimate rooseli db
      postgresUser = "postgres";
      # sfaDbPath = ./db/pgdump_2022-11-30.sql;
      pgSettings = {
        psql =  pkgs.postgresql.withPackages (p: [ p.postgis ]);
        tmp  = "/tmp/postgres/";
        data = "postgres_data";
      };
      testWrongUser = ''
        echo "sesame will not open"
        curl -v -X POST -H "Content-Type: application/json" \
            -d '{"username":"Ali Baba","password":"Open Sesamo"}' \
            http://127.0.0.1:8000/auth/login/
      '';
      testUser = ''
        echo "sending alibaba"
        curl -v -X POST -H "Content-Type: application/json" \
            -d '{"username":"Ali Baba","password":"Open Sesame"}' \
            http://127.0.0.1:8000/auth/login/
      '';
      addUser = ''
        NAME=Stadtbüro
        EMAIL=kontakt@stadtfueralle.info
        ! [ -v $1 ] && NAME=$1
        ! [ -v $2 ] && EMAIL=$2
        echo "sending user: $NAME with email: $EMAIL"
        curl -X POST -H "Content-Type: application/json" \
            -d "{\"name\": \"$NAME\", \"email\": \"$EMAIL\"}" \
            http://127.0.0.1:8000/api/user/create/
      '';

      llPaths = with pkgs;
        map (x: (callPackage x { inherit stdenv fetchzip; }).outPath) (import ./leaflet.nix);
      llPathsText = with builtins;
        concatStringsSep " " llPaths;

      hp = pkgs.haskellPackages;
      lxc = import ./lxc.nix { inherit pkgs; inherit system; };

    in rec {
      inherit lxc;
      projectDrv = pkgs;
      defaultPackage.${system} = pkgs.rooseliExec;
      packages.${system} = {
        meta = lxc.meta;
        geoDb = (nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            { nixpkgs = { inherit pkgs; }; }
            lxc.geoDbConfig
            "${nixpkgs}/nixos/modules/virtualisation/lxc-container.nix"
          ];
        }).config.system.build.tarball;
        server = (nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            { nixpkgs = { inherit pkgs; }; }
            lxc.serverConfig
            "${nixpkgs}/nixos/modules/virtualisation/lxc-container.nix"
          ];
        }).config.system.build.tarball;
        nginx = (nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            { nixpkgs = { inherit pkgs; }; }
            lxc.nginxConfig
            "${nixpkgs}/nixos/modules/virtualisation/lxc-container.nix"
          ];
        }).config.system.build.tarball;
        lxcHost = (nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            { nixpkgs = { inherit pkgs; }; }
            "${nixpkgs}/nixos/modules/virtualisation/qemu-vm.nix"
            (import ./qemu.nix "10.13.12")
          ];
        }).config.system.build.vm;
      };
      devShell.${system} = hp.shellFor {
        packages = p: [ pkgs.rooseli ];
        buildInputs = with pkgs; [
          hp.apply-refact hp.hlint hp.stylish-haskell hp.hasktags hp.hoogle
          hp.cabal-install gdb
          lxc.generalCmds
          (lxc.appCmds "server")
          (lxc.appCmds "geoDb")
          (lxc.appCmds "nginx")
          lxd
          gdal
          git
          osm2pgsql
          osmium-tool
          pgSettings.psql
          (writeShellScriptBin "runServerInGhci" ''
            ghci -iserver/src/
          '')
          (writeShellScriptBin "copyOsmAdminTableToRooseliDb" ''
            pg_dump -h /tmp/postgres -U postgres -t osm_admin rawgeo | psql -h /tmp/postgres -U postgres rooseli
          '')
          (writeShellScriptBin "collectStatic" ''
            cp -r server/css server/js server/img static/
                      '')
          (writeShellScriptBin "addUser" addUser)
          (writeShellScriptBin "testUser" testUser)
          (writeShellScriptBin "testWrongUser" testWrongUser)
          (writeShellScriptBin "pgStart" ''
            ${pgSettings.psql}/bin/pg_ctl start -D ${pgSettings.data} -l ${pgSettings.tmp + "LOG"} \
              -o "-c listen_addresses= -c unix_socket_directories=${pgSettings.tmp}"
            '')
          (writeShellScriptBin "initRooseliDb" ''
            echo "init rooseli db"
            ${pgSettings.psql}/bin/createdb -h ${pgSettings.tmp} -U postgres ${rooseliDbName}
            ${pgSettings.psql}/bin/psql -h ${pgSettings.tmp} -U postgres ${rooseliDbName} -c "CREATE EXTENSION postgis;"
          '')
          (writeShellScriptBin "connectRasDb" ''
            psql -U mapper1 -h 193.148.249.132 -p 15432 postgres
          '')
          (writeShellScriptBin "dumpLocalRooseli" ''
            pg_dump -U postgres -h /tmp/postgres rooseli > "db/dumpRooseli_$(date +%Y-%m-%d).sql"
          '')
          (writeShellScriptBin "restoreRooseliDbToIfog" ''
            psql -U postgres -d rooseli -h 193.148.249.132 -p 15433 -f db/dumpRooseli_2022-12-07.sql
          '')
          (writeShellScriptBin "dumpSfaDb" ''
            pg_dump -U mapper1 -h 193.148.249.132 -p 15432 postgres > "db/pgdump_$(date +%Y-%m-%d).sql"
          '')
        ];
        withHoogle = true;
        shellHook =
          ''
            export PS1='✊\u@rooselish \$ '
            mkdir -p ./static/js/leaflet
            for lllib in ${llPathsText}; do
                cp -ruv $lllib/* static/js/
            done
            chmod -R u+w static/js
            if [ ! -d ${pgSettings.tmp} ]; then
              mkdir -p ${pgSettings.tmp}
            fi

            source ./server/env.sh
            source ./server/secret.sh
            if [ ! -d ${pgSettings.data} ]; then
              echo 'Initializing postgresql database...'
              ${pgSettings.psql}/bin/initdb ${pgSettings.data} -U ${postgresUser} --auth=trust --locale en_US.UTF-8 >/dev/null
              pgStart
              initRawGeoDb
              initRooseliDb
            fi
            echo "Checking postgres status..."
            ${pgSettings.psql}/bin/pg_ctl -D ${pgSettings.data} status
            if [ $? -eq 3 ]; then
              pgStart
            fi
          '';
      };
    };
}
