[
  ({ stdenv, fetchzip }: stdenv.mkDerivation rec {
    pname = "leaflet";
    version = "1.7.1";
    src = fetchzip {
      url = "http://cdn.leafletjs.com/${pname}/v${version}/leaflet.zip";
      sha256 = "sha256-NQ7PGVUKMzTXu6Falu8Kd7jghoEqtjCIA31ExUIYfDs=";
      stripRoot = false;
    };
    installPhase = ''
          mkdir -p $out
          cp -vr $src $out/${pname}
        '';
  })
  ({ stdenv, fetchzip }: stdenv.mkDerivation rec {
    pname = "Leaflet.markercluster";
    version = "1.4.1";
    src = fetchzip {
      url = "https://github.com/Leaflet/${pname}/archive/v${version}.zip";
      sha256 = "06ml684b1n2355i2fbp0ghx4fpzr3cjmnkbv0jzb3c5na2dfwdki";
      stripRoot = false;
    };
    installPhase = ''
          mkdir -p $out
          cp -vr $src/${pname}-${version}/dist $out/${pname}
        '';
  })
]
