{ pkgs, system }:
let
  project = "rooseli";
  nixpkgs = pkgs.lib.cleanSource pkgs.path;
  fishEnvVarFromInitProc = pkgs.writeShellScript "fishVar" ''
    ${pkgs.findutils}/bin/xargs -n 1 -0 < /proc/1/environ  | ${pkgs.gnused}/bin/sed -n "s/^$1=\(.*\)/\1/p"
  '';

  exportVars = pkgs.writeShellScriptBin "exportVars" ''
    export APIHOST=$(${fishEnvVarFromInitProc} APIHOST)
    export DBNAME=$(${fishEnvVarFromInitProc} DBNAME)
    export DBPASS=$(${fishEnvVarFromInitProc} DBPASS)
    export DBHOST=$(${fishEnvVarFromInitProc} DBHOST)
    export DBPORT=$(${fishEnvVarFromInitProc} DBPORT)
    export DBUSER=$(${fishEnvVarFromInitProc} DBUSER)
    export ENVIRONMENT=$(${fishEnvVarFromInitProc} ENVIRONMENT)
    export OIDC_CLIENT_ID=$(${fishEnvVarFromInitProc} OIDC_CLIENT_ID)
    export OIDC_CLIENT_SECRET=$(${fishEnvVarFromInitProc} OIDC_CLIENT_SECRET)
  '';

in {
  nginxConfig = { pkgs, config, ... }: {
    networking.defaultGateway = {
      address = "10.0.13.11"; # ip of ifog host net device
      interface = "eth0";
    };
    networking.nameservers = [ "45.134.88.88" ]; # ifog nameserver
    networking.interfaces.eth0.ipv4.addresses = [{
      address = "10.0.13.3";
      prefixLength = 24;
    }];
    networking.firewall.allowedTCPPorts = [ 80 443 ];
    networking.hostName = "rooseli-nginx";
    networking.useDHCP = false;
    services.nginx = {
      enable = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      virtualHosts = {
        "localhost" = {
          locations."/static/".root = "/www";
          locations."/".proxyPass = "http://10.0.13.1:8000";
        };
        "map.stadtfueralle.info" = {
          addSSL = true;
          enableACME = true;
          locations."/static/".root = "/www";
          locations."/".proxyPass = "http://10.0.13.1:8000";
        };
      };
    };
    security.acme.email = "news@durga.ch";
    security.acme.acceptTerms = true;
    services.openssh.enable = true;
    users.extraUsers.root.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOf6dgKch+BWz8MbOG6iovPhQQgrx4GRnm78HR3NnayP (none)"
    ];
  };

  geoDbConfig = { pkgs, config, ... }: {
    networking.firewall.allowedTCPPorts = [ 22 5432 ];
    networking.hostName = "geoDb";
    networking.interfaces.eth0.ipv4.addresses = [{
      address = "10.0.13.93";
      prefixLength = 24;
    }];
    networking.useDHCP = false;
    environment.systemPackages = with pkgs; [ vim ];
    users = {
      mutableUsers = false;
    };
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql;
      enableTCPIP = true;
      authentication = pkgs.lib.mkOverride 10 ''
        local all      all               trust
        host  rooseli  postgres 0.0.0.0/0 md5
        host  postgres postgres 0.0.0.0/0 md5
      '';
      extraPlugins = with pkgs.postgresql.pkgs; [ postgis ];
      ensureDatabases = ["rooseli"];
      # within flake I have to track this file if I want to do this:
      # initialScript = ./db/dumpRooseli_2022-10-06.sql;
    };
    users.extraUsers.root.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOf6dgKch+BWz8MbOG6iovPhQQgrx4GRnm78HR3NnayP (none)"
    ];
    services.sshd.enable = true;
    services.openssh.enable = true;
  };

  serverConfig = { pkgs, config, ... }: {
    boot.postBootCommands = ''
      # allow systemd to start user service after boot, no login needed
      loginctl enable-linger rooseli
    '';
    environment.systemPackages = [
      pkgs.rooseliExec
    ];
    networking.defaultGateway = {
      address = "10.0.13.11";
      interface = "eth0";
    };
    networking.firewall.allowedTCPPorts = [ 8000 ];
    networking.hostName = "rooseli-server";
    networking.interfaces.eth0.ipv4.addresses = [{
      address = "10.0.13.1";
      prefixLength = 24;
    }];
    networking.nameservers = [ "45.134.88.88" ]; # ifog dns
    networking.useDHCP = false;
    users.users.rooseli = {
      isNormalUser = true;
      uid = 1000;
    };
    users.extraUsers.root = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOf6dgKch+BWz8MbOG6iovPhQQgrx4GRnm78HR3NnayP (none)"
      ];
    };
    services.openssh.enable = true;
    systemd.services.runserver = {
      enable = true;
      # preStart = ""; # sourcing here does not work, script gets a new environment
      script = "source ${exportVars}/bin/exportVars && ${pkgs.su}/bin/su -c ${pkgs.rooseliExec}/bin/run-server rooseli";
      serviceConfig = {
        Restart = "always";
        ExecStop = "${pkgs.utillinux}/bin/kill -9 $MAINPID";
        Type = "exec";
      };
      wantedBy = [ "default.target" ];
    };
  };

  meta = pkgs.stdenv.mkDerivation {
    name = "tarball";
    phases = [ "installPhase" ];
    installPhase = ''
      cat << EOF > metadata.yaml
      architecture: x86_64
      creation_date: 1617404749
      properties:
        description: NixOS
        os: NixOS
        release: unknown
      EOF
      mkdir $out
      tar -cv --mtime='@1' --owner=0 --group=0 --numeric-owner -c metadata.yaml | pixz > $out/metadata.tar.xz
    '';
    nativeBuildInputs = [ pkgs.gnutar pkgs.pixz ];
  };

  appCmds = appName: [
    (pkgs.writeShellScriptBin "lxcImport-${appName}" ''
      lxc image import lxcBuilds/meta/metadata.tar.xz lxcBuilds/${appName}/tarball/nixos-system-x86_64-linux.tar.xz yoyo: --alias ${appName}
    '')
    (pkgs.writeShellScriptBin "lxcWipe-${appName}" ''
      lxc stop yoyo:${appName}
      lxc delete yoyo:${appName}
      lxc image delete yoyo:${appName}
    '')
    (pkgs.writeShellScriptBin "lxcLaunch-${appName}" ''
      lxc launch yoyo:${appName} -p ${project} yoyo:${appName}
    '')
  ];
  generalCmds = [
    (pkgs.writeShellScriptBin "createBridge" ''
      ip tuntap add mode tap user cri name ${project}Tap
      ip link set ${project}Tap up
      ip link set ${project}Tap master br0
    '')
    (pkgs.writeShellScriptBin "lxcCreatePool" ''
      lxc storage create yoyo:pool1 btrfs
    '')
    (pkgs.writeShellScriptBin "lxcLoadServerProfile" ''
      lxc profile create yoyo:server || true
      lxc profile edit yoyo:server < ${pkgs.writeText "profile.yaml" ''
        config:
          environment.DBNAME: rooseli
          environment.DBPASS: postgres
          environment.DBHOST: 10.0.13.93
          environment.DBPORT: 5432
          environment.DBUSER: postgres
        description:  rooseli server profile
        name: server
        ''}
    '')
    (pkgs.writeShellScriptBin "lxcLoadNginxProfile" ''
      lxc profile create yoyo:nginx || true
      lxc profile edit yoyo:nginx < ${pkgs.writeText "profile.yaml" ''
        description:  ${project} common profile
        devices:
          staticdir:
            path: /www/static/
            source: /static/
            type: disk
        name: nginx
        ''}
    '')
    (pkgs.writeShellScriptBin "lxcLoadCommonProfile" ''
      lxc profile create yoyo:common || true
      lxc profile edit yoyo:common < ${pkgs.writeText "profile.yaml" ''
        description:  ${project} common profile
        devices:
          root:
            path: /
            pool: pool1
            type: disk
          eth0:
            name: eth0
            nictype: bridged
            parent: br0
            type: nic
        name: common
        ''}
    '')
  ];
}
