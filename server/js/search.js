const searchBox = document.getElementById('result-list')
const searchInput = document.getElementById('search-input')
const searchBtn = document.getElementById('search-btn')

const calcZoomOptions = () => {
    // --------- <-appbar->-------
    // |-------|           |-----|
    // |     | |           |     |
    // |     | |           |     |
    // ---------           |-----|
    //        ^----mapbar->-------
    const o = window.innerWidth/window.innerHeight
    const mapbarW = document.getElementById('mapbar').offsetWidth
    const mapbarH = document.getElementById('mapbar').offsetHeight
    const appbarW = document.getElementById('appbar').offsetWidth
    const appbarH = document.getElementById('appbar').offsetHeight
    return {
        // o > 1 -> landscape:
        //          padding of mapbar width from the right
        //                     appbar height from the top
        // o < 1 -> portrait:
        //          padding of mapbar height from the bottom
        //                     appbar height from the top
        paddingBottomRight: (o > 1) ? [ mapbarW, 0 ] : [ 0, mapbarH ],
        paddingTopLeft:     (o > 1) ? [ 0, appbarH ] : [ 0, appbarH ],
        animate: true,
        duration: 2
    }
}

let debounceTimer = undefined
const timeout = 500

const processSearchInput = () => {
    document.getElementById(`search-btn`).classList.add('close')
    document.getElementById(`search-btn`).classList.remove('open')
    searchBox.innerHTML = ''
    if (debounceTimer) clearTimeout(debounceTimer)
    debounceTimer = setTimeout(() => {
        Object.entries(features).map(([obj, ftParams]) => {
            const propKeyL = Object.keys(ftParams.searchProps)
            const layer = search (searchInput.value) (obj) (propKeyL)
            if (layer.length > 0) {
                const featFind = getFrame ('feature-find')
                featFind.querySelector('[itemprop=object]').textContent = obj
                layer.map(([key, value]) => {
                    const propL = displayFind (obj) (value)
                    propL.map(p => featFind.querySelector('ul').appendChild(p))
                })
                searchBox.appendChild(featFind)
            }
        })
        debounceTimer = undefined
    }, timeout);
}

const resetSearch = () => {
    searchInput.value = ''
    searchBox.innerHTML = ''
    searchBtn.classList.add('open')
    searchBtn.classList.remove('close')
}

const search = token => obj => propKeyL => {
    let hits = Object.entries(ftCollection[obj]._layers).filter(
        ([key, value]) => {
            const res = propKeyL.reduce((acc, prop) => {
                const val = String (value.feature.properties[prop])
                return (acc || val.includes (token))
            }, false)
            return res
        }
    )
    return hits
}

const displayFind = obj => value => {
    const propKeyL = Object.entries(features[obj].searchProps)
    return propKeyL.map(([key, val]) => {
        const propFrame = getFrame ('prop-find')
        propFrame.querySelector('.label').textContent = `${val}: `
        propFrame.querySelector('.value').textContent =
            value.feature.properties[key]
        if (obj === 'eingang') Object.entries(ftCollection.gebaeude._layers)
            .filter(([key, l]) =>
                l.feature.properties.egid === value.feature.properties.egid)
            .map(([key, l]) => {
                propFrame.onmouseover = () => selectGeba (l)
                propFrame.onclick = () => {
                    clickGebaeude (l)
                    map.fitBounds(l.getBounds(), calcZoomOptions());
                    resetSearch ()
                }
            })
        else if (obj == 'gebaeude') {
            propFrame.onmouseover = () => selectGeba (value)
            propFrame.onclick = () => {
                clickGebaeude (value)
                map.fitBounds(value.getBounds(), calcZoomOptions ());
                resetSearch ()
            }
        }
        else if (obj === 'liegenschaft' || obj === 'baurecht') {
            propFrame.onmouseover = () => selectParz (value)
            propFrame.onclick = () => {
                clickParzelle (value)
                map.fitBounds(value.getBounds(), calcZoomOptions ());
                resetSearch ()
            }
        }
        return propFrame
    })
}
