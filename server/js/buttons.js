const handleForm = htmlId => {
    const form = document.querySelector(`.${htmlId}.edit`)
    const inputs = form.querySelectorAll(".value")
    let obj = {}
    inputs.forEach(i => {
        const k = i.getAttribute("name")
        const v = i.value
        const t = i.getAttribute("data-value-type")
        if      (t === "number") obj[k] = Number(v)
        else if (t === "date")   obj[k] = (new Date(v)).toISOString()
        else                     obj[k] = v
    })
    return obj
}

const httpRequest = async (url, opts) => {
	  const response = await fetch(url, opts);
	  if (!response.ok) {
		    const errorMessage = await response.text();
		    throw new Error(errorMessage);
	  }
	  return response;
}

const chooseMe = (id, name) => {
    const overall = document.querySelector('#overall-dialog')
    const button = overall._target
    button.value = id
    button.innerHTML = name
    overall.classList.add('blank')
}

const openChooser = async (target, object) => {
    const overall = document.querySelector('#overall-dialog')
    overall.classList.remove('blank')
    overall._target = target
    const fetchOptions = {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "text/html; charset=UTF-8",
            "Accept": "text/html"
        },
	  };
    try {
        const resp = await httpRequest(`/${object}/chooser`, fetchOptions)
        overall.querySelector('.window').innerHTML = await resp.text()
    } catch (e) {
        console.log(e)
    }
}

const saveMe = async (id, object) => {
    // abort if form is not valid
    const form = document.querySelector(`form.${object}-${id}`)
    if (!form.checkValidity()) {
        form.querySelectorAll('.invalid').forEach(e => {
            e.classList.remove('invalid')
            e.parentNode.querySelector('.error-msg').innerHTML = ""
        })
        form.querySelectorAll(':invalid').forEach(e => {
            e.classList.add('invalid')
            e.parentNode.querySelector('.error-msg').innerHTML = e.validationMessage
        })
        return
    }

    const payload = handleForm(`${object}-${id}`)
    let fetchOptions = {}
    let url
    if (id === 0) {
        fetchOptions = {
            method: "POST",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(payload),
        }
        url = `/api/${object}/create`
    } else {
        fetchOptions = {
            method: "PATCH",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(payload),
        }
        url = `/api/${object}/update/${id}`
    }
    const fetchOptionsText = {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "text/html; charset=UTF-8",
            "Accept": "text/html"
        },
	  };
    try {
        const resp = await httpRequest (url, fetchOptions)
        const retId = await resp.text()
        const display = await httpRequest (`/${object}/snippet/get/${retId}`, fetchOptionsText)
        const editHtml = await display.text()
        const elem = document.querySelector(`.${object}-${id}.edit`)
        elem.parentNode.innerHTML = editHtml
    } catch (e) {
        console.log(`Error: ${e}`)
    }
}

const createEmptyForm = async (object) => {
    if (document.querySelector(`.${object}-0`)) {
        console.log('There is an empty form already.')
        return
    }
    const fetchOptions = {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "text/html; charset=UTF-8",
            "Accept": "text/html"
        },
	  };
    try {
        const resp = await httpRequest (`/${object}/snippet/create`, fetchOptions)
        const editHtml = await resp.text()
        const liDiv = document.createElement('li');
        liDiv.innerHTML = editHtml
        const elem = document.querySelector('.sfadata.list ul')
        elem.prepend(liDiv)
        liDiv.focus()
        // document.getElementById(htmlElemId).parentNode.innerHTML = editHtml
    } catch (e) {
        console.log(`Error: ${e}`)
    } finally {
        return
    }
}

const editMe = async (id, object) => {
    const fetchOptions = {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "text/html; charset=UTF-8",
            "Accept": "text/html"
        },
	  };
    try {
        const resp = await httpRequest (`/${object}/snippet/update/${id}`, fetchOptions)
        const editHtml = await resp.text()
        const elem = document.querySelector(`.${object}-${id}.display`)
        const parent = elem.parentNode
        parent.innerHTML = editHtml
        parent.appendChild(elem)
        elem.classList.add('blank')
        // document.getElementById(htmlElemId).parentNode.innerHTML = editHtml
    } catch (e) {
        console.log(`Error: ${e}`)
    }
}

const withdrawMe = async (id, object) => {
    const htmlId = `${object}-${id}`
    const editEl = document.querySelector(`.${htmlId}.edit`)
    if (id === '0' || id === 0) {
        const editElPar = editEl.parentNode
        editElPar.parentNode.removeChild(editElPar)
        return
    }
    const displayEl = document.querySelector(`.${htmlId}.display`)
    displayEl.classList.remove('blank')
    displayEl.parentNode.removeChild(editEl)
}

const deleteMe = async (id, object) => {
    if (!window.confirm(`${object} mit id ${id} wirklich löschen?`)) return
    const fetchOptions = {
        method: "DELETE",
        credentials: "same-origin",
    }
    try {
        const resp = await httpRequest (`/api/${object}/delete/${id}`, fetchOptions)
        const liEl = document.querySelector(`.${object}-${id}.display`).parentNode
        // parent of li -> ul, remove the whole li Element
        liEl.parentNode.removeChild(liEl)
    } catch (e) {
        handleError(e)
    }
}
