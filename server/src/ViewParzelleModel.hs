{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module ViewParzelleModel ( Eigentum (..)
                          , KategorieEigentum (..)
                          , Besitzende (..)
                          ) where


import           Data.Aeson   (FromJSON, ToJSON)
import           Data.Int     (Int64)
import qualified Data.Text    as T
import           Data.Time    (UTCTime)
import           GHC.Generics (Generic)

data Eigentum = Eigentum
  { eId        :: Int64
  , eDatum     :: UTCTime
  , eParzEgrid :: T.Text
  , eKatId     :: Int64
  , eKatName   :: T.Text
  , eBemerk    :: T.Text
  , eBesitz    :: T.Text
  , eBesId     :: Maybe Int64
  , eBesName   :: Maybe T.Text
  , eBesBemerk :: Maybe T.Text
  , eBesOrt    :: Maybe T.Text
  } deriving (Show, Generic, ToJSON, FromJSON)

data KategorieEigentum = KategorieEigentum
  { kId   :: Int64
  , kName :: T.Text
  } deriving (Show, Generic, ToJSON, FromJSON)

data Besitzende = Besitzende
  { bId        :: Int64
  , bName      :: T.Text
  , bBemerk    :: T.Text
  , bStrasseNr :: T.Text
  , bPlz       :: T.Text
  , bOrt       :: T.Text
  , bLand      :: Maybe T.Text
  } deriving (Show, Generic, ToJSON, FromJSON)
