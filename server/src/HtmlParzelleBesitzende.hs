{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlParzelleBesitzende ( besDisplay
                              , besListDisplay
                              , besChooser
                              , besForm
                              ) where

import           Data.Maybe        (isJust)
import qualified Data.Text         as T
import           Lucid

import           BasicSchema       (User)
import           HtmlGeneric       (chooseFromList, deleteButton, editButton,
                                    itemDisplay, lTxtInput, listDisplay,
                                    sTxtInput, saveButton, withdrawButton)
import           ViewGeneric       (SfaData, toDisplay)
import           ViewParzelleModel (Besitzende (..))



besChooser :: SfaData Besitzende => [Besitzende] -> Html ()
besChooser kelist = chooseFromList $ formBesitzende <$> kelist
  where
    formBesitzende :: Besitzende -> (T.Text, T.Text, Html())
    formBesitzende b = (T.pack . show $ bId b, bName b, toDisplay Nothing b)

besListDisplay :: SfaData Besitzende
               => Maybe User -> [Besitzende] -> Html ()
besListDisplay mbUsr keL = listDisplay $ toDisplay mbUsr <$> keL

besDisplay :: Maybe User -> Besitzende -> Html ()
besDisplay mbUser bes =
  let object           = "parzelle-besitzende"
      isEditable       = isJust mbUser
      (id', name, bemerk, strnr, plz, ort, _)
                       = serializeBes $ Just bes
  in figure_ [ class_ $ T.concat [ "card display "
                                 , object, "-", id'
                                 ] ] $ do
    figcaption_ [] $ do
      if isEditable
        then div_ [ class_ "register"] $ do
               div_ [ class_ "edit-buttons" ] $ do
                  editButton id' object
                  deleteButton id' object
        else mempty
    ul_ [] $ do
      itemDisplay "text" Nothing False $ Just name
      itemDisplay "str" Nothing False $ Just strnr
      itemDisplay "str" Nothing False $ Just plz
      itemDisplay "str" Nothing False $ Just ort
      itemDisplay "text" Nothing False $ Just bemerk

besForm :: Maybe Besitzende -> Html ()
besForm bes =
  let object           = "parzelle-besitzende"
      (id', name, bemerk, strnr, plz, ort, land)
                       = serializeBes bes
  in form_ [ class_ $ T.concat [ "edit ", object, "-", id' ] ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          div_ [ class_ "edit-buttons" ] $ do
            saveButton id' object
            withdrawButton id' object
      ul_ [] $ do
        li_ [ data_ "type" "text" ] $ do
          sTxtInput True id' "besitzendeName" (Just "Name") (Just name)
        li_ [ data_ "type" "text" ] $ do
          lTxtInput True id' "besitzendeBemerkung" (Just "Bemerkung") (Just bemerk)
        li_ [ data_ "type" "str" ] $ do
          sTxtInput True id' "besitzendeStrasse_nr" (Just "Anschrift") (Just strnr)
        li_ [ data_ "type" "str" ] $ do
          sTxtInput True id' "besitzendeOrt" (Just "Ort") (Just ort)
        li_ [ data_ "type" "str" ] $ do
          sTxtInput True id' "besitzendePlz" (Just "Postleitzahl") (Just plz)
        li_ [ data_ "type" "str" ] $ do
          sTxtInput False id' "besitzendeLand_nr" (Just "Land") land

serializeBes :: Maybe Besitzende -> ( T.Text, T.Text, T.Text, T.Text
                                    , T.Text, T.Text, Maybe T.Text
                                    )
serializeBes = maybe ("0", "", "", "", "", "", Just "")
                     (\b -> ( T.pack . show $ bId b
                            , bName b
                            , bBemerk b
                            , bStrasseNr b
                            , bPlz b
                            , bOrt b
                            , bLand b
                            ))
