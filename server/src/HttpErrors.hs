{-# LANGUAGE OverloadedStrings #-}

module HttpErrors (unauthorized) where

import           CommonLucid (appWrapper)
import           Servant     (ServerError, err401, errBody, errHeaders)


unauthorized :: ServerError
unauthorized = err401 { errBody = appWrapper Nothing "Nicht erlaubt"
                      , errHeaders = [("Content-Type", "text/html;charset=utf-8")]
                      }
