{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module ViewGebaeude ( gebByEgid
                    ) where

import           Control.Monad.IO.Class          (liftIO)
import           Control.Monad.Trans.Except      (throwE)
import           Data.Maybe                      (maybeToList)

import           Database.Esqueleto.Experimental

import           Lucid                           (renderBS)
import           Servant                         (Handler (..), err401, errBody)


import           BasicSchema                     (User)
import           CommonLucid                     (RawHtml (..))
import qualified Database                        as Db
import qualified DatabaseGebaeude                as DbGeb
import qualified DbRooseli                       as DbR
import           HtmlGebaeude                    (gebaeudeDisplay)
import           HtmlGebaeudeInfo                (infoDisplay, infoForm,
                                                  infoListDisplay)
import           HtmlGebaeudeKategorieInfo       (katInfoChooser,
                                                  katInfoDisplay, katInfoForm,
                                                  katInfoListDisplay)
import qualified SchemaGebaeude                  as SchG
import           ViewGebaeudeModel               (Info (..), KategorieInfo (..))
import           ViewGeneric                     (OneToMany (..), SfaData (..))

instance OneToMany KategorieInfo where
  chooseOneFromList = katInfoChooser

instance SfaData Info where
  name        = "gebaeude-info"
  getById id' = fmap (assembleInfo <$>) (DbGeb.qInfoById id')
  getList     = fmap (assembleInfo <$>) DbGeb.qInfoList
  emptySfa    = infoForm Nothing
  toFormSfa i = infoForm $ Just i
  toDisplay   = infoDisplay
  toList      = infoListDisplay

instance SfaData KategorieInfo where
  name        = "gebaeude-kategorieinfo"
  getById id'  = do
    mbRec <- Db.qRecordById id'
    pure $ KategorieInfo id' . SchG.kategorieInfoKategorie <$> maybeToList mbRec
  getList      = do
    keyRecL <- (Db.peelKeyRecord <$>) <$> Db.qRecordList
    pure $ (\keyRec -> KategorieInfo (fromSqlKey $ fst keyRec)
                                     (SchG.kategorieInfoKategorie $ snd keyRec)
           ) <$> keyRecL
  emptySfa     = katInfoForm Nothing
  toFormSfa    = katInfoForm . Just
  toDisplay    = katInfoDisplay
  toList       = katInfoListDisplay


assembleInfo :: (Entity SchG.Info , Entity SchG.KategorieInfo) -> Info
assembleInfo (Entity iKey i, Entity katKey kat) =
                                      Info (fromSqlKey iKey)
                                           (SchG.infoDatum i)
                                           (SchG.infoGebaeudeEgid i)
                                           (SchG.infoInfo i)
                                           (SchG.infoAbsender i)
                                           (fromSqlKey katKey)
                                           (SchG.kategorieInfoKategorie kat)

handleGebaeude :: Maybe User -> SchG.Gebaeude -> Handler RawHtml
handleGebaeude mbUsr g = do
  eingangL <- liftIO . Db.peelValues $ DbR.qEingangByGebaeude g
  infoL    <- liftIO $ (assembleInfo <$>) <$> DbGeb.qInfoByEgid (SchG.gebaeudeEgid g)
  pure . RawHtml . renderBS $ gebaeudeDisplay mbUsr g infoL eingangL

gebByEgid :: Maybe User -> Double -> Handler RawHtml
gebByEgid mbUsr egid = do
  mbGeb <- liftIO $ DbR.qGebaeudeByEgid egid
  case mbGeb of
    Just (Entity _ val') -> handleGebaeude mbUsr val'
    Nothing              -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find Gebaeude with that egid" }
