{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}


module RouteGebaeude where

import           Servant             (Capture, Get, Server, (:<|>) (..), (:>))
import           Servant.Auth.Server (Auth, AuthResult (Authenticated),
                                      throwAll)

import           BasicSchema         (User)
import           CommonLucid         (HTML, RawHtml (..))
import           HttpErrors          (unauthorized)
import           ViewGebaeude        (gebByEgid)
import           ViewGebaeudeModel   (Info, KategorieInfo)
import           ViewGeneric         (AppRoute, ChooseOneRoute, SnippetRoute,
                                      appServer, chooserServer, snippetServer)

type InfoRoute    = AppRoute Info :<|>
                    SnippetRoute Info

type KatInfoRoute = AppRoute KategorieInfo :<|>
                    SnippetRoute KategorieInfo :<|>
                    ChooseOneRoute KategorieInfo

katInfoServer :: User -> Server KatInfoRoute
katInfoServer usr = appServer usr :<|> snippetServer usr :<|> chooserServer

infoServer :: User -> Server InfoRoute
infoServer usr = appServer usr :<|> snippetServer usr

type Route = "gebaeude-info"          :> InfoRoute
        :<|> "gebaeude-kategorieinfo" :> KatInfoRoute
        :<|> "gebaeudeByEgid"         :> Capture "egid" Double
                                      :> Get '[HTML] RawHtml

type API auths = Auth auths User :> Route

server :: AuthResult User -> Server Route
server (Authenticated usr) =
  infoServer usr  :<|> katInfoServer usr :<|> gebByEgid (Just usr)
server _ =
  throwAll unauthorized :<|> throwAll unauthorized :<|> gebByEgid Nothing
