{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module ViewParzelle ( byEgrid
                    ) where

import           Control.Monad.IO.Class          (liftIO)
import           Control.Monad.Trans.Except      (throwE)
import           Data.Int                        (Int64)
import           Data.Maybe                      (maybeToList)

import qualified Data.Text                       as T
import           Database.Esqueleto.Experimental

import           Lucid                           (renderBS)
import           Servant                         (Handler (..), err401, errBody)


import           BasicSchema                     (User)
import           CommonLucid                     (RawHtml (..))
import           Database                        as Db
import qualified DatabaseParzelle                as DbParz
import           HtmlParzelle                    (parzDisplay)
import           HtmlParzelleBesitzende          (besChooser, besDisplay,
                                                  besForm, besListDisplay)
import           HtmlParzelleEigentum            (display, eigListDisplay, form)
import           HtmlParzelleKategorieEigentum   (katDisplay, katEigenChooser,
                                                  katForm, katListDisplay)
import qualified SchemaParzelle                  as SchP
import           ViewGeneric                     (OneToMany (..), SfaData (..))
import           ViewParzelleModel               (Besitzende (..),
                                                  Eigentum (..),
                                                  KategorieEigentum (..))

instance OneToMany KategorieEigentum where
  chooseOneFromList = katEigenChooser

instance OneToMany Besitzende where
  chooseOneFromList = besChooser

instance SfaData Eigentum where
  name        = "parzelle-eigentum"
  getById id' = fmap (assembleEigentum <$>) (DbParz.qEigentumById id')
  getList     = fmap (assembleEigentum <$>) DbParz.qEigentumList
  emptySfa    = form Nothing
  toFormSfa i = form $ Just i
  toDisplay   = display
  toList      = eigListDisplay

instance SfaData Besitzende where
  name        = "parzelle-besitzende"
  getById id'  = do
    mbRec <- Db.qRecordById id'
    pure $ assembleBesitzende id' <$> maybeToList mbRec
  getList      = do
    keyRecL <- (Db.peelKeyRecord <$>) <$> Db.qRecordList
    pure $ (\keyRec -> assembleBesitzende
                       (fromSqlKey $ fst keyRec)
                       (snd keyRec)
           ) <$> keyRecL
  emptySfa     = besForm Nothing
  toFormSfa    = besForm . Just
  toDisplay    = besDisplay
  toList       = besListDisplay

instance SfaData KategorieEigentum where
  name         = "parzelle-kategorieeigentum"
  getById id'  = do
    mbRec <- Db.qRecordById id'
    pure $ KategorieEigentum id' . SchP.kategorieEigentumKategorie
        <$> maybeToList mbRec
  getList      = do
    keyRecL <- (Db.peelKeyRecord <$>) <$> Db.qRecordList
    pure $ (\keyRec -> KategorieEigentum
                        (fromSqlKey $ fst keyRec)
                        (SchP.kategorieEigentumKategorie $ snd keyRec)
           ) <$> keyRecL
  emptySfa     = katForm Nothing
  toFormSfa    = katForm . Just
  toDisplay    = katDisplay
  toList       = katListDisplay

assembleBesitzende :: Int64 -> SchP.Besitzende -> Besitzende
assembleBesitzende i b = Besitzende i (SchP.besitzendeName b)
                                      (SchP.besitzendeBemerkung b)
                                      (SchP.besitzendeStrasse_nr b)
                                      (SchP.besitzendePlz b)
                                      (SchP.besitzendeOrt b)
                                      (SchP.besitzendeLand b)

assembleEigentum :: ( Entity SchP.Eigentum
                    , Maybe (Entity SchP.Besitzende)
                    , Entity SchP.KategorieEigentum
                    ) -> Eigentum
assembleEigentum (Entity iKey i, bes, Entity katKey kat) =
  Eigentum (fromSqlKey iKey)
           (SchP.eigentumDatum i)
           (SchP.eigentumParzelleEgrid i)
           (fromSqlKey katKey)
           (SchP.kategorieEigentumKategorie kat)
           (SchP.eigentumBemerkung i)
           (SchP.eigentumBesitz i)
           ((\(Entity key _)  -> fromSqlKey key)                <$> bes)
           ((\(Entity _ valu) -> SchP.besitzendeName valu)      <$> bes)
           ((\(Entity _ valu) -> SchP.besitzendeBemerkung valu) <$> bes)
           ((\(Entity _ valu) -> SchP.besitzendeOrt valu)       <$> bes)


handleParzelle :: Maybe User -> SchP.Parzelle -> Handler RawHtml
handleParzelle mbUsr p = do
  eigentumL <- liftIO $ (assembleEigentum <$>) <$> DbParz.qEigentumByEgrid (SchP.parzelleEgrid p)
  pure . RawHtml . renderBS $ parzDisplay mbUsr p eigentumL

byEgrid :: Maybe User -> T.Text -> Handler RawHtml
byEgrid mbUsr egrid = do
  mbParz <- liftIO $ DbParz.qParzelleByEgrid egrid
  case mbParz of
    Just (Entity _ val') -> handleParzelle mbUsr val'
    Nothing              -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find Parzelle with that egrid" }
