{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}

module DatabaseEsq ( qEigentumByEgrid ) where

import           Database.Esqueleto.Experimental

import           Data.ByteString.Char8           (pack)
import qualified Data.Text                       as T

import qualified Database                        as Db
import qualified SchemaParzelle                  as SchP


qEigentumByEgrid :: T.Text -> IO [( Entity SchP.Eigentum
                                  , Maybe (Entity SchP.Besitzende)
                                  , Entity SchP.KategorieEigentum
                                  )]
qEigentumByEgrid egrid = do
  cString <- pack <$> Db.rooseliConnString
  Db.runAction cString $
    select $ do
    (eigen :& besitz :& kat) <-
      from $ table @SchP.Eigentum
      `leftJoin` table @SchP.Besitzende
      `on` (\(eigen :& besitz) ->
        besitz ?. SchP.BesitzendeId ==. eigen ^. SchP.EigentumBesitzende)
      `innerJoin` table @SchP.KategorieEigentum
      `on` (\(eigen :& _ :& kat) ->
        kat ^. SchP.KategorieEigentumId ==. eigen ^. SchP.EigentumKategorie)
    where_ (eigen ^. SchP.EigentumParzelleEgrid ==. val egrid)
    orderBy [ desc (eigen ^. SchP.EigentumDatum) ]
    pure (eigen, besitz, kat)

