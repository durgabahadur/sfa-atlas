{-# LANGUAGE AllowAmbiguousTypes   #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeOperators         #-}

module ViewGeneric where

import           Control.Monad.IO.Class     (liftIO)
import           Control.Monad.Trans.Except (throwE)
import           Data.Int                   (Int64)
import           Data.Text                  (Text)

import           Lucid
import           Network.HTTP.Media         ((//), (/:))

import           Servant                    (Accept (..), Capture, Get,
                                             Handler (..), MimeRender (..),
                                             Server, err401, errBody,
                                             (:<|>) (..), (:>))

import           BasicSchema                (User)
import           CommonLucid                (HTML, RawHtml (..), appWrapper)
import           HtmlGeneric                (importItemSearchJs, itemSearchBox,
                                             newButton)

data SnippetDisplay
data SnippetForm
data AppForm
data AppDisplay
data ChooseOne

type AppRoute a = "list"   :> Get '[AppDisplay] (User, [a])
             :<|> "get"    :> Capture "id" Int64
                           :> Get '[AppDisplay] (User, a)
             :<|> "create" :> Get '[HTML] RawHtml
             :<|> "update" :> Capture "id" Int64
                           :> Get '[AppForm] (User, a)

type ChooseOneRoute a = "chooser" :> Get '[ChooseOne] [a]

type SnippetRoute a = "snippet" :>
    ( "list"   :> Get '[SnippetDisplay] (User, [a])
 :<|> "get"    :> Capture "id" Int64
               :> Get '[SnippetDisplay] (User, a)
 :<|> "create" :> Get '[HTML] RawHtml
 :<|> "update" :> Capture "id" Int64
               :> Get '[SnippetForm] a
    )

class SfaData a => OneToMany a where
  chooseOneFromList :: [a] -> Html ()

class SfaData a where
  name           :: Text
  getById        :: Int64 -> IO [a]
  getList        :: IO [a]
  emptySfa       :: Html ()
  toFormSfa      :: a -> Html ()
  toDisplay      :: Maybe User -> a -> Html ()
  toList         :: Maybe User -> [a] -> Html ()

instance Accept ChooseOne where
  contentType _ = "text" // "html" /: ("charset", "utf-8")

instance Accept AppDisplay where
  contentType _ = "text" // "html" /: ("charset", "utf-8")

instance Accept AppForm where
  contentType _ = "text" // "html" /: ("charset", "utf-8")

instance Accept SnippetDisplay where
  contentType _ = "text" // "html" /: ("charset", "utf-8")

instance Accept SnippetForm where
  contentType _ = "text" // "html" /: ("charset", "utf-8")

instance (OneToMany a) => MimeRender ChooseOne [a] where
    mimeRender _ l = renderBS $ chooseOneFromList l

instance (SfaData a) => MimeRender AppForm (User, a) where
    mimeRender _ (usr, a) = appWrapper (Just usr) $ toFormSfa a

instance {-# OVERLAPPABLE #-} (SfaData a)
  => MimeRender AppDisplay (User, a)
  where mimeRender _ (usr, a) = appWrapper (Just usr) $ toDisplay (Just usr) a

instance {-# OVERLAPPING #-} (SfaData a)
  => MimeRender AppDisplay (User, [a])
  where mimeRender _ (usr, l) = appWrapper (Just usr) $
          figure_ [id_ "sfadata-view"] $ do
            figcaption_ [] $ do
              div_ [ class_ "flex-wrapper"] $ do
                itemSearchBox
                newButton (name @a)
            toList (Just usr) l
            importItemSearchJs

instance (SfaData a) => MimeRender SnippetForm a where
    mimeRender _ a = renderBS $ toFormSfa a

instance {-# OVERLAPPABLE #-} (SfaData a)
  => MimeRender SnippetDisplay (User, a) where
    mimeRender _ (usr, a) = renderBS $ toDisplay (Just usr) a

instance {-# OVERLAPPING #-} (SfaData a)
  => MimeRender SnippetDisplay (User, [a]) where
    mimeRender _ (usr, l) = renderBS $ toList (Just usr) l

listHandler :: SfaData a => User -> Handler (User, [a])
listHandler usr = do
  aList <- liftIO getList
  pure (usr, aList)

byIdUsr :: SfaData a => User -> Int64 -> Handler (User, a)
byIdUsr u id' = do
  byId <- byId' id'
  pure (u, byId)

byId' :: SfaData a => Int64 -> Handler a
byId' id' = do
  info <- liftIO $ getById id'
  case info of
    i:_ -> liftIO $ pure i
    _   -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find record with that ID" }

create :: forall a . SfaData a => Handler RawHtml
create = pure $ RawHtml $ renderBS $ emptySfa @a

appCreateHtml :: forall a . SfaData a => User -> Handler RawHtml
appCreateHtml usr = do
  pure $ RawHtml $ appWrapper (Just usr) $ emptySfa @a

appServer :: forall a . SfaData a => User -> Server (AppRoute a)
appServer usr = listHandler usr
           :<|> byIdUsr usr
           :<|> appCreateHtml @a usr
           :<|> byIdUsr usr

chooserServer :: forall a . OneToMany a
              => Server (ChooseOneRoute a)
chooserServer = liftIO getList

snippetServer :: forall a . SfaData a
              => User
              -> Server (SnippetRoute a)
snippetServer usr = listHandler usr
                               :<|> byIdUsr usr
                               :<|> create @a
                               :<|> byId'
