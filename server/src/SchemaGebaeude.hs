{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module SchemaGebaeude where

import           Data.Aeson                  (FromJSON, ToJSON, object, toJSON,
                                              (.=))
import           Data.Geometry.Geos.Geometry (MultiPolygon (..), Some (Some))
import qualified Data.Text                   as T
import           Data.Time                   (UTCTime)
import qualified Database.Persist.TH         as PTH
import           GHC.Generics                (Generic)

import           ApiGeneric                  (ApiData)
import           PostgisHaskellBinding       (Geo)

PTH.share [PTH.mkPersist PTH.sqlSettings, PTH.mkMigrate "migrateAll"] [PTH.persistLowerCase|
Gebaeude sql=gebaeude
  geom (Geo MultiPolygon)
  egid Double
  status T.Text
  egrid T.Text
  baujahr Int Maybe
  abbruchjahr Int Maybe
  flaeche Int Maybe
  volumen Int Maybe
  bezeich T.Text Maybe
  kategor T.Text Maybe
  anzahlgeschosse Int Maybe
  anzahlwohnung Int Maybe
  created UTCTime default=CURRENT_TIMESTAMP
  UniqueEgid egid
  deriving Generic Show

KategorieInfo sql=kategorie_info
  kategorie T.Text
  UniqueKategorieInfo kategorie
  deriving Show Generic ToJSON FromJSON

Info sql=info
  datum UTCTime default=CURRENT_TIMESTAMP
  gebaeudeEgid Double
  info T.Text
  absender T.Text Maybe
  kategorie KategorieInfoId
  Foreign Gebaeude fk_info_gebaeude gebaeudeEgid References egid
  UniqueInfo datum gebaeudeEgid kategorie
  deriving Show Generic ToJSON FromJSON
|]

instance ApiData Info
instance ApiData KategorieInfo

-- | GeoJson representation of gebaeude
instance ToJSON Gebaeude where
  toJSON g = object
    [ "geometry"    .= Some (gebaeudeGeom g)
    , "type"        .= ("Feature" :: T.Text)
    , "properties"  .= object
      [ "egid"            .= gebaeudeEgid g
      , "baujahr"         .= gebaeudeBaujahr g
      , "flaeche" .= gebaeudeFlaeche g
      , "volumen" .= gebaeudeVolumen g
      , "bezeich" .= gebaeudeBezeich g
      , "kategor" .= gebaeudeKategor g
      , "anzahlgeschosse" .= gebaeudeAnzahlgeschosse g
      , "anzahlwohnung"   .= gebaeudeAnzahlwohnung g
      ]
    ]
