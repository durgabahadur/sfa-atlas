{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlParzelleEigentum ( form
                            , display
                            , eigListDisplay
                            ) where

import           Data.Maybe        (fromMaybe, isJust)
import qualified Data.Text         as T
import           Data.Time         (showGregorian, utctDay)
import           Lucid

import           BasicSchema       (User)
import           HtmlGeneric       (deleteButton, editButton, inputDate,
                                    itemDisplay, listDisplay, openChooserButton,
                                    sTxtInput, saveButton, withdrawButton)
import           ViewGeneric       (SfaData, toDisplay)
import           ViewParzelleModel (Eigentum (..))

form :: Maybe Eigentum -> Html ()
form mbEigen =
  let object = "parzelle-eigentum"
      (id', datum, egrid, katId, kat, bemerk, besitz, besId, besName, _, _)
             = serializeEigen mbEigen
  in form_ [ class_ $ T.concat [ "edit ", object, "-", id' ] ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [ ] $ do
        div_ [ class_ "register" ] $ do
          div_ [ data_ "type" "date" ] $ do
            inputDate "datum" "eigentumDatum" Nothing True (Just datum)
          div_ [ class_ "edit-buttons" ] $ do
            saveButton id' object
            withdrawButton id' object
      ul_ [] $ do
        li_ [ data_ "type" "str"] $ do
          sTxtInput True "parzelle-egrid" "eigentumParzelleEgrid"
            (Just "Parzelle Egrid")(Just egrid)
        li_ [ data_ "type" "str"] $ do
          openChooserButton "eigentum-kategorie" "eigentumKategorie"
            (Just "Kategorie") True (katId, kat) "parzelle-kategorieeigentum"
        li_ [ data_ "type" "str" ] $ do
          sTxtInput True "besitz" "eigentumBesitz"
            (Just "Besitz Kommentar") (Just besitz)
        li_ [ data_ "type" "str" ] $ do
          sTxtInput True "bemerkung" "eigentumBemerkung"
            (Just "Bemerkung") (Just bemerk)
        li_ [ data_ "type" "str"] $ do
          openChooserButton "eigentum-besitzende" "eigentumBesitzende"
            (Just "Besitzende") True ( fromMaybe "0" besId
                                     , fromMaybe "" besName
                                     ) "parzelle-besitzende"


eigListDisplay :: SfaData Eigentum => Maybe User -> [Eigentum] -> Html ()
eigListDisplay mbUsr iL = listDisplay (toDisplay mbUsr <$> iL)

display :: Maybe User -> Eigentum -> Html ()
display mbUser e =
  let
    object           = "parzelle-eigentum"
    isEditable       = isJust mbUser
    (id', datum, _, _, katName, bemerk, besitz, _, besName, besBemerk, besOrt)
                     = serializeEigen $ Just e
    showConfidential = isJust mbUser || (katName /= "Privatperson")
  in figure_ [ class_ $ T.concat [ "card display " , object, "-", id']
             ] $ do
       figcaption_ [ class_ "head" ] $ do
         div_ [ data_ "type" "date", class_ "register" ] $ do
           span_ [ class_ "value" ] (toHtml datum)
           if isEditable
           then div_ [ class_ "edit-buttons" ] $ do
                  editButton id' object
                  deleteButton id' object
           else mempty
       ul_ [] $ do
         -- itemDisplay "str" (Just "Parzelle Egrid") False $ Just egrid
         itemDisplay "str" (Just "Kategorie") False $ Just katName
         itemDisplay "str" (Just "Bemerkung Eigentum") False $ Just bemerk
         if showConfidential
           then do
             itemDisplay "text" (Just "Name")             False besName
             itemDisplay "str"  (Just "Besitz")           False $ Just besitz
             itemDisplay "str"  (Just "Ort")              False besOrt
             itemDisplay "text" (Just "Bemerkung Besitz") False besBemerk
           else
             itemDisplay "str" (Just "Besitz") False (Just "Privatperson")



serializeEigen :: Maybe Eigentum -> ( T.Text, T.Text, T.Text, T.Text
                                    , T.Text, T.Text, T.Text
                                    , Maybe T.Text, Maybe T.Text
                                    , Maybe T.Text, Maybe T.Text
                                    )
serializeEigen = maybe ("0", "", "", "", "", "", "", Nothing, Nothing, Nothing, Nothing)
                      (\e -> ( T.pack . show $ eId e
                             , T.pack . showGregorian . utctDay $ eDatum e
                             , eParzEgrid e
                             , T.pack . show $ eKatId e
                             , eKatName e
                             , eBemerk e
                             , eBesitz e
                             , T.pack . show <$> eBesId e
                             , eBesName e
                             , eBesBemerk e
                             , eBesOrt e
                             ))
