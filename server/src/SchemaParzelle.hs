{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module SchemaParzelle where

import           Data.Aeson                  (FromJSON, ToJSON, object, toJSON,
                                              (.=))
import           Data.Geometry.Geos.Geometry (MultiPolygon (..), Some (Some))
import qualified Data.Text                   as T
import           Data.Time
import qualified Database.Persist.TH         as PTH
import           GHC.Generics                (Generic)

import           ApiGeneric                  (ApiData)
import           PostgisHaskellBinding       (Geo)

PTH.share [PTH.mkPersist PTH.sqlSettings, PTH.mkMigrate "migrateAll"] [PTH.persistLowerCase|
Parzelle sql=parzelle
  geom (Geo MultiPolygon)
  egrid T.Text
  typ T.Text
  art T.Text
  area Double
  grundbuchkreisnummer T.Text
  grundstuecksnummer T.Text
  created UTCTime default=CURRENT_TIMESTAMP
  UniqueEgrid egrid
  deriving Generic Show

Besitzende sql=besitzende
  name T.Text
  strasse_nr T.Text
  plz T.Text
  ort T.Text
  land T.Text Maybe
  bemerkung T.Text
  UniqueBesitzende name bemerkung
  deriving Generic Show ToJSON FromJSON

KategorieEigentum sql=kategorie_eigentum
  kategorie T.Text
  UniqueKategorieEigentum kategorie
  deriving Show Generic ToJSON FromJSON

Eigentum sql=eigentum
  datum UTCTime default=CURRENT_TIMESTAMP
  parzelleEgrid T.Text
  besitz T.Text
  besitzende BesitzendeId Maybe
  bemerkung T.Text
  kategorie KategorieEigentumId
  Foreign Parzelle fk_eigentum_parzelle parzelleEgrid References egrid
  UniqueEigentum datum parzelleEgrid besitz
  deriving Show Generic ToJSON FromJSON
|]

instance ApiData Besitzende
instance ApiData KategorieEigentum
instance ApiData Eigentum

-- | create a GeoJSON Feature
instance ToJSON Parzelle where
  toJSON p = object
    [ "geometry"    .= Some (parzelleGeom p)
    , "type"        .= ("Feature" :: String)
    , "properties"  .= object
      [ "egrid" .= parzelleEgrid p
      , "typ"   .= parzelleTyp p
      , "area"  .= parzelleArea p
      ]
    ]
