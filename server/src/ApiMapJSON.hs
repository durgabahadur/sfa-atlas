{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module ApiMapJSON where

import           Control.Monad.IO.Class               (liftIO)
import           Control.Monad.Trans.Except           (throwE)
import           Data.Aeson                           (ToJSON, object, toJSON,
                                                       (.=))
import           Data.Int                             (Int64)
import qualified Data.Text                            as T
import           Database.Persist.Class.PersistEntity (Entity (..))
import           Database.Persist.Sql                 (fromSqlKey)
import           Servant                              (Capture, Get,
                                                       Handler (..), JSON,
                                                       Server, err401, errBody,
                                                       (:<|>) (..), (:>))

import qualified Database                             as Db
import qualified DatabaseEsq                          as DbEsq
import qualified DatabaseGebaeude                     as DbG
import qualified DatabaseParzelle                     as DbP
import qualified DbRooseli                            as DbR
import qualified SchemaEingang                        as SchE
import qualified SchemaGebaeude                       as SchG
import qualified SchemaParzelle                       as SchP

type API = "parzelleByEgrid" :> Capture "egrid" T.Text
                             :> Get '[JSON] Parzelle
      :<|> "gebaeudeByEgid"  :> Capture "egid" Double
                             :> Get '[JSON] Gebaeude

data Parzelle = Parzelle
  { parzelle    :: SchP.Parzelle
  , parEigentum :: [Eigentum]
  } deriving (Show)

instance ToJSON Parzelle where
  toJSON p = object
    [ "egrid"    .= SchP.parzelleEgrid (parzelle p)
    , "typ"      .= SchP.parzelleTyp (parzelle p)
    , "area"     .= SchP.parzelleArea (parzelle p)
    , "eigentum" .= toJSON (parEigentum p)
    ]

data Gebaeude = Gebaeude
  { gebaeude   :: SchG.Gebaeude
  , gebEingang :: [Eingang]
  , gebInfo    :: [Info]
  } deriving (Show)

instance ToJSON Gebaeude where
  toJSON g = object
    [ "baujahr"         .= SchG.gebaeudeBaujahr (gebaeude g)
    , "abbruchjahr"     .= SchG.gebaeudeAbbruchjahr (gebaeude g)
    , "egid"            .= SchG.gebaeudeEgid (gebaeude g)
    , "kategor"         .= SchG.gebaeudeKategor (gebaeude g)
    , "flaeche"         .= SchG.gebaeudeFlaeche (gebaeude g)
    , "volumen"         .= SchG.gebaeudeVolumen (gebaeude g)
    , "anzahlwohnung"   .= SchG.gebaeudeAnzahlwohnung (gebaeude g)
    , "info"            .= toJSON (gebInfo g)
    , "eingang"         .= toJSON (gebEingang g)
    ]

newtype Eingang = Eingang { eingang :: SchE.Eingang } deriving Show

instance ToJSON Eingang where
  toJSON e =
    let
      str = SchE.eingangStrasse (eingang e)
      nr  = SchE.eingangNummer (eingang e)
    in
      object ["strnr" .= T.unwords [str, nr]]

-- | sfa data
data Eigentum = Eigentum
  { eigentumId   :: Int64
  , eigentum     :: SchP.Eigentum
  , eigKategorie :: SchP.KategorieEigentum
  , besitzendeId :: Maybe Int64
  , besitzende   :: Maybe SchP.Besitzende
  } deriving (Show)

instance ToJSON Eigentum where
  toJSON e = object
    [ "datum"     .= SchP.eigentumDatum (eigentum e)
    , "bemerkung" .= SchP.eigentumBemerkung (eigentum e)
    , "besitz"    .= SchP.eigentumBesitz (eigentum e)
    , "kategorie" .= SchP.kategorieEigentumKategorie (eigKategorie e)
    , "besOrt"    .= maybe "" SchP.besitzendeOrt (besitzende e)
    , "besName"   .= maybe "" SchP.besitzendeName (besitzende e)
    , "besBemerk" .= maybe "" SchP.besitzendeBemerkung (besitzende e)
    ]

data Info = Info
  { infoId       :: Int64
  , info         :: SchG.Info
  , infKategorie :: SchG.KategorieInfo
  } deriving (Show)

instance ToJSON Info where
  toJSON i = object
    [ "datum"     .= SchG.infoDatum (info i)
    , "kategorie" .= SchG.kategorieInfoKategorie (infKategorie i)
    , "info"      .= SchG.infoInfo (info i)
    , "absender"  .= SchG.infoAbsender (info i)
    ]

handleInfo :: (Entity SchG.Info, Entity SchG.KategorieInfo) -> Info
handleInfo (Entity infKey infVal, Entity _ katVal) =
  Info (fromSqlKey infKey) infVal katVal

handleGebaeude :: SchG.Gebaeude -> IO Gebaeude
handleGebaeude g = do
  eingangL <- Db.peelValues $ DbR.qEingangByGebaeude g
  infoL    <- DbG.qInfoByEgid (SchG.gebaeudeEgid g)
  pure $ Gebaeude g (Eingang <$> eingangL) (handleInfo <$> infoL)

handleEigen
  :: ( Entity SchP.Eigentum
     , Maybe (Entity SchP.Besitzende)
     , Entity SchP.KategorieEigentum
     )
  -> Eigentum
handleEigen (Entity eigKey eigVal, mbBesitzende, Entity _ katVal) =
  case mbBesitzende of
    Just (Entity besKey besVal)
      -> Eigentum (fromSqlKey eigKey)
                  eigVal
                  katVal
                  (Just $ fromSqlKey besKey)
                  (Just besVal)
    Nothing
      -> Eigentum (fromSqlKey eigKey)
                  eigVal
                  katVal
                  Nothing
                  Nothing

handleParzelle :: SchP.Parzelle -> IO Parzelle
handleParzelle p = do
  eigRawL <- DbEsq.qEigentumByEgrid $ SchP.parzelleEgrid p
  pure $ Parzelle p (handleEigen <$> eigRawL)

parHandler :: T.Text -> Handler Parzelle
parHandler egrid = do
  mbParz <- liftIO $ DbP.qParzelleByEgrid egrid
  case mbParz of
    Just (Entity _ val) -> liftIO $ handleParzelle val
    Nothing             -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find Parzelle with that egrid" }

gebHandler :: Double -> Handler Gebaeude
gebHandler egid = do
  mbGeb <- liftIO $ DbR.qGebaeudeByEgid egid
  case mbGeb of
    Just (Entity _ val) -> liftIO $ handleGebaeude val
    Nothing             -> Handler err
  where
    err = throwE $ err401 { errBody = "Could not find Gebaeude with that egid" }

server :: Server API
server = parHandler :<|> gebHandler
