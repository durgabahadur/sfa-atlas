{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlGebaeudeKategorieInfo ( katInfoDisplay
                                 , katInfoListDisplay
                                 , katInfoChooser
                                 , katInfoForm) where

import           Data.Maybe        (isJust)
import qualified Data.Text         as T
import           Lucid

import           BasicSchema       (User)
import           HtmlGeneric       (chooseFromList, deleteButton, editButton,
                                    itemDisplay, listDisplay, sTxtInput,
                                    saveButton, withdrawButton)
import           ViewGebaeudeModel (KategorieInfo (..))
import           ViewGeneric       (SfaData, toDisplay)

katInfoChooser :: SfaData KategorieInfo => [KategorieInfo] -> Html ()
katInfoChooser katInfoL = chooseFromList $ shapeKatInfo <$> katInfoL
  where
    shapeKatInfo :: KategorieInfo -> (T.Text, T.Text, Html())
    shapeKatInfo ki = (T.pack . show $ kId ki, kName ki, toDisplay Nothing ki)

katInfoListDisplay :: SfaData KategorieInfo
                   => Maybe User -> [KategorieInfo] -> Html ()
katInfoListDisplay mbUsr kiL = listDisplay $ toDisplay mbUsr <$> kiL

katInfoDisplay :: Maybe User -> KategorieInfo -> Html ()
katInfoDisplay mbUser mbKatInfo =
  let object           = "gebaeude-kategorieinfo"
      isEditable       = isJust mbUser
      (id', kat)       = serializeKatInfo $ Just mbKatInfo
  in figure_ [ class_ $ T.concat [ "card display " , object, "-", id']
             ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          if isEditable
            then div_ [ class_ "edit-buttons" ] $ do
              editButton id' object
              deleteButton id' object
            else mempty
      ul_ [] $ do
        itemDisplay "str" Nothing False $ Just kat

katInfoForm :: Maybe KategorieInfo -> Html ()
katInfoForm mbKatInfo =
  let object           = "gebaeude-kategorieinfo"
      (id', kat)       = serializeKatInfo mbKatInfo
  in form_ [ class_ $ T.concat [ "edit ", object, "-", id' ] ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          div_ [ class_ "edit-buttons" ] $ do
            saveButton id' object
            withdrawButton id' object
      ul_ [] $ do
        li_ [ data_ "type" "text" ] $ do
          sTxtInput True id' "kategorieInfoKategorie" Nothing (Just kat)

serializeKatInfo :: Maybe KategorieInfo -> ( T.Text, T.Text)
serializeKatInfo = maybe ("0", "") (\ki -> (T.pack . show $ kId ki, kName ki))
