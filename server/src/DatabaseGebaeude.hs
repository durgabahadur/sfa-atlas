{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeOperators     #-}

module DatabaseGebaeude ( infoQuery
                        , qInfoList
                        , qInfoById
                        , qInfoByEgid
                        , qGebaeudeWithoutEingang
                        ) where

import           Database.Esqueleto.Experimental

import           Data.ByteString.Char8           (pack)
import           Data.Int                        (Int64)

import qualified Database                        as Db
import qualified SchemaEingang                   as SchE
import qualified SchemaGebaeude                  as SchG

-- shortcut because used heavily
type InfoL = [(Entity SchG.Info, Entity SchG.KategorieInfo)]

infoQuery :: SqlQuery (SqlExpr (Entity SchG.Info)
                         :& SqlExpr (Entity SchG.KategorieInfo))
infoQuery = from $ table @SchG.Info
              `innerJoin` table @SchG.KategorieInfo
              `on` (\(info_ :& kat) ->
                info_ ^. SchG.InfoKategorie ==. kat ^. SchG.KategorieInfoId)

qInfoList :: IO InfoL
qInfoList = Db.runActionR $
                select $ do
                (info_ :& kat) <- infoQuery
                orderBy [ desc (info_ ^. SchG.InfoDatum) ]
                pure (info_, kat)

qInfoById :: Int64 -> IO InfoL
qInfoById key = Db.runActionR $
    select $ do
    (info_ :& kat) <- infoQuery
    where_ ((info_ ^. SchG.InfoId) ==. val (toSqlKey key))
    pure (info_, kat)

qInfoByEgid :: Double -> IO InfoL
qInfoByEgid egid = Db.runActionR $
    select $ do
    (info_ :& kat) <-
        from $ table @SchG.Info
        `innerJoin` table @SchG.KategorieInfo
        `on` (\(info_ :& kat) ->
          info_ ^. SchG.InfoKategorie ==. kat ^. SchG.KategorieInfoId)
    where_ (info_ ^. SchG.InfoGebaeudeEgid ==. val egid)
    orderBy [ desc (info_ ^. SchG.InfoDatum) ]
    pure (info_, kat)

qGebaeudeWithoutEingang :: IO [Entity SchG.Gebaeude]
qGebaeudeWithoutEingang = do
  cString <- pack <$> Db.rooseliConnString
  Db.runAction cString $
    select $ do
    (geb :& eing) <-
        from $ table @SchG.Gebaeude
        `leftJoin` table @SchE.Eingang
        `on` (\(geb :& eing) ->
          just (geb ^. SchG.GebaeudeEgid) ==. eing ?. SchE.EingangGebaeudeEgid)
    where_ (isNothing $ eing ?. SchE.EingangId)
    pure geb
