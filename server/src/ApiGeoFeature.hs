{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}

module ApiGeoFeature (
                     API, server
                     )where

import           Control.Monad.IO.Class (liftIO)
import           Data.Aeson             (ToJSON, object, toJSON, (.=))
import           Data.Maybe             (fromMaybe)
import qualified Data.Text              as T
import           GHC.TypeLits
import           Servant

import qualified Database               as Db

import qualified SchemaEingang          as SchE
import qualified SchemaGebaeude         as SchG
import qualified SchemaParzelle         as SchP

-- | GeoJSON features provided by rooseli
data GeoFeature = EinFeature SchE.Eingang
                | GebFeature SchG.Gebaeude
                | ParFeature SchP.Parzelle

instance ToJSON GeoFeature where
  toJSON (EinFeature fc) = toJSON fc
  toJSON (GebFeature fc) = toJSON fc
  toJSON (ParFeature fc) = toJSON fc

-- | List of features aka GeoJSON feature collection served by api
newtype FeatureCollection = FeatureCollection { features :: [GeoFeature] }

instance ToJSON FeatureCollection where
  toJSON fc = object
    [ "type" .= ("FeatureCollection" :: String)
    , "features"  .= (toJSON <$> features fc)
    ]
-- | an endpoint named after a geo object. takes a region as query parameter and
-- | returns a json value (in fact a GeoJson FeatureCollection)
type GenericAPI (name :: Symbol) = name :>
  QueryParam "region" String :> Get '[JSON] FeatureCollection

type API = GenericAPI "liegenschaft"
      :<|> GenericAPI "baurecht"
      :<|> GenericAPI "gebaeude"
      :<|> GenericAPI "eingang"

parzellenHandler :: T.Text -> Maybe String -> Handler FeatureCollection
parzellenHandler typ mbRegion = do
  qParz <- liftIO $ Db.peelValues $
    Db.qGeoInRegion "parzelle" (T.pack $ fromMaybe "Basel" mbRegion)
  let pFilter = case typ of
        "Baurecht"     -> \p -> SchP.parzelleTyp p == "Baurecht"
        "Liegenschaft" -> \p -> SchP.parzelleTyp p == "Liegenschaft"
                             && SchP.parzelleArt p == "Liegenschaft"
  pure . FeatureCollection $ ParFeature <$> filter pFilter qParz

gebaeudeHandler :: Maybe String -> Handler FeatureCollection
gebaeudeHandler mbRegion = do
  qGeb <- liftIO $ Db.peelValues $
    Db.qGeoInRegion "gebaeude" (T.pack $ fromMaybe "Basel" mbRegion)
  pure . FeatureCollection $ GebFeature <$> qGeb

eingangHandler :: Maybe String -> Handler FeatureCollection
eingangHandler mbRegion = do
  qEingang <- liftIO $ Db.peelValues $
    Db.qGeoInRegion "eingang" (T.pack $ fromMaybe "Basel" mbRegion)
  pure . FeatureCollection $ EinFeature <$> qEingang

server :: Server API
server = parzellenHandler "Liegenschaft"
    :<|> parzellenHandler "Baurecht"
    :<|> gebaeudeHandler
    :<|> eingangHandler
