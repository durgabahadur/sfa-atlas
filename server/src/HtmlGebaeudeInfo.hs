{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlGebaeudeInfo ( infoForm
                        , infoDisplay
                        , infoListDisplay
                        ) where

import           Data.Double.Conversion.Text (toFixed)
import           Data.Maybe                  (isJust)
import qualified Data.Text                   as T
import           Data.Time                   (showGregorian, utctDay)
import           Lucid

import           BasicSchema                 (User)
import           HtmlGeneric                 (deleteButton, editButton,
                                              inputDate, inputNumber,
                                              itemDisplay, lTxtInput,
                                              listDisplay, openChooserButton,
                                              sTxtInput, saveButton,
                                              withdrawButton)
import           ViewGebaeudeModel           (Info (..))
import           ViewGeneric                 (SfaData, toDisplay)

infoForm :: Maybe Info -> Html ()
infoForm mbInfo =
  let object = "gebaeude-info"
      (id', datum, egid, absender, bemerk, kat, katId)
             = serializeInfo mbInfo
  in form_ [ class_ $ T.concat [ "edit ", object, "-", id' ] ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [ ] $ do
        div_ [ class_ "register" ] $ do
          div_ [ data_ "type" "date" ] $ do
            inputDate "datum" "infoDatum" Nothing True (Just datum)
          div_ [ class_ "edit-buttons" ] $ do
            saveButton id' object
            withdrawButton id' object
      ul_ [] $ do
        li_ [ data_ "type" "str"] $ do
          inputNumber "gebaeude-egid" "infoGebaeudeEgid"
                      (Just "Gebaeude Egid") True (Just egid)
        li_ [ data_ "type" "str"] $ do
          openChooserButton "info-kategorie" "infoKategorie" (Just "Kategorie")
                            True (katId, kat) "gebaeude-kategorieinfo"
        li_ [ data_ "type" "str" ] $ do
          sTxtInput False "absender" "infoAbsender" (Just "Absender") absender
        li_ [ data_ "type" "text"] $ do
          lTxtInput True "bemerkung" "infoInfo" (Just "Bemerkung") (Just bemerk)


infoListDisplay :: SfaData Info => Maybe User -> [Info] -> Html ()
infoListDisplay mbUsr iL = listDisplay (toDisplay mbUsr <$> iL)

infoDisplay :: Maybe User -> Info -> Html ()
infoDisplay mbUser i =
  let
    object           = "gebaeude-info"
    isEditable       = isJust mbUser
    editButtonsClass = if isEditable then "edit-buttons"
                                     else "edit-buttons blank"
    (id', datum, egid, absender, bemerk, kat, _) = serializeInfo $ Just i
  in figure_ [ class_ $ T.concat [ "card display ", object, "-", id']] $ do
       figcaption_ [ ] $ do
         div_ [ data_ "type" "date", class_ "register" ] $ do
           span_ [ class_ "value" ] (toHtml datum)
           div_ [ class_ editButtonsClass ] $ do
             editButton id' object
             deleteButton id' object
       ul_ [] $ do
         itemDisplay "str" (Just "Gebaeude Egid") False $ Just egid
         itemDisplay "str" (Just "Kategorie") False $ Just kat
         itemDisplay "str" (Just "Absender") False absender
         itemDisplay "text" (Just "Bemerkung") False $ Just bemerk


serializeInfo :: Maybe Info -> ( T.Text, T.Text, T.Text, Maybe T.Text
                               , T.Text, T.Text, T.Text)
serializeInfo = maybe ("0", "", "", Nothing, "", "", "")
                      (\i -> ( T.pack. show $ iId i
                             , T.pack . showGregorian . utctDay $ iDatum i
                             , toFixed 0 $ iGebEgid i
                             , iAbsender i
                             , iText i
                             , iKatName i
                             , T.pack. show $ iKatId i
                             ))
