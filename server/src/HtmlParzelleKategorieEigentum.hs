{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlParzelleKategorieEigentum ( katDisplay
                                     , katListDisplay
                                     , katEigenChooser
                                     , katForm) where

import           Data.Maybe        (isJust)
import qualified Data.Text         as T
import           Lucid

import           BasicSchema       (User)
import           HtmlGeneric       (chooseFromList, deleteButton, editButton,
                                    itemDisplay, listDisplay, sTxtInput,
                                    saveButton, withdrawButton)
import           ViewGeneric       (SfaData, toDisplay)
import           ViewParzelleModel (KategorieEigentum (..))



katEigenChooser :: SfaData KategorieEigentum => [KategorieEigentum] -> Html ()
katEigenChooser kelist = chooseFromList $ formKatEigen <$> kelist
  where
    formKatEigen :: KategorieEigentum -> (T.Text, T.Text, Html())
    formKatEigen ke = (T.pack . show $ kId ke, kName ke, toDisplay Nothing ke)

katListDisplay :: SfaData KategorieEigentum
               => Maybe User -> [KategorieEigentum] -> Html ()
katListDisplay mbUsr keL = listDisplay $ toDisplay mbUsr <$> keL

katDisplay :: Maybe User -> KategorieEigentum -> Html ()
katDisplay mbUser mbKatEigen =
  let object           = "parzelle-kategorieeigentum"
      isEditable       = isJust mbUser
      (id', kat)       = serializeKat $ Just mbKatEigen
  in figure_ [ class_ $ T.concat [ "card display " , object, "-", id']
             ] $ do
        figcaption_ [] $ do
          if isEditable
            then div_ [ class_ "register"] $ do
                   div_ [ class_ "edit-buttons" ] $ do
                     editButton id' object
                     deleteButton id' object
            else mempty
        ul_ [] $ do
          itemDisplay "text" Nothing False $ Just kat

katForm :: Maybe KategorieEigentum -> Html ()
katForm mbKatEigen =
  let object           = "parzelle-kategorieeigentum"
      (id', kat)       = serializeKat mbKatEigen
  in form_ [ class_ $ T.concat [ "edit ", object, "-", id' ] ] $ do
    figure_ [ class_ "card" ] $ do
      figcaption_ [] $ do
        div_ [ class_ "register"] $ do
          div_ [ class_ "edit-buttons" ] $ do
            saveButton id' object
            withdrawButton id' object
      ul_ [] $ do
        li_ [ data_ "type" "text" ] $ do
          sTxtInput True id' "kategorieEigentumKategorie" Nothing (Just kat)

serializeKat :: Maybe KategorieEigentum -> ( T.Text, T.Text)
serializeKat = maybe ("0", "") (\ke -> (T.pack . show $ kId ke, kName ke))
