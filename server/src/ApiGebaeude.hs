{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module ApiGebaeude where

import           Servant        (Server, (:<|>) (..), (:>))

import qualified ApiGeneric     as Generic
import           SchemaGebaeude (Info (..), KategorieInfo)

type API auths = "gebaeude-info"          :> Generic.API Info auths
            :<|> "gebaeude-kategorieinfo" :> Generic.API KategorieInfo auths

server :: Server (API auths)
server = Generic.server :<|> Generic.server
