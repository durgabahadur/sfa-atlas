{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TypeFamilies      #-}

module Database where

import           Control.Monad.Logger
import           Control.Monad.Reader            (runReaderT)
import           Data.ByteString.Char8           (pack)
import           Data.Geometry.Geos.Geometry     (Geometry (MultiPolygonGeometry, PointGeometry),
                                                  MultiPolygon, Point)
import           Data.Geometry.Geos.Serialize    (writeHex)
import           Data.Int                        (Int64)
import           Data.String.Here                (i, iTrim)
import           Data.Text                       (Text)
import           Database.Esqueleto.Experimental
import           Database.Persist.Postgresql
import qualified Database.Persist.Sql            as Sql
import qualified Database.Persist.TH             as PTH
import           System.Environment

import           PostgisHaskellBinding           (Geo (..))

type PGInfo = ConnectionString

localConnString :: PGInfo
localConnString = "host=/tmp/postgres/rooseli.socket port=5432 user=postgres dbname=postgres password=postgres"

-- access to fresh postgres 11 db, several dbs are running here
connString :: String -> IO String
connString dbName = do
  host <- getEnv "DBHOST"
  port <- getEnv "DBPORT"
  user <- getEnv "DBUSER"
  pass <- getEnv "DBPASS"
  return $ "host="++ host
        ++ " port=" ++ port
        ++ " user=" ++ user
        ++ " dbname=" ++ dbName
        ++ " password=" ++ pass

-- access to fresh postgres 11 db, several dbs are running here
rooseliConnString :: IO String
rooseliConnString = do
  host <- getEnv "DBHOST"
  port <- getEnv "DBPORT"
  user <- getEnv "DBUSER"
  pass <- getEnv "DBPASS"
  return $ "host="++ host
       ++ " port=" ++ port
       ++ " user=" ++ user
       ++ " dbname=" ++ "rooseli"
       ++ " password=" ++ pass

-- the old sfa db is running on postgres 10
sfaConnString :: IO String
sfaConnString = do
  host <- getEnv "SFA_DBHOST"
  port <- getEnv "SFA_DBPORT"
  user <- getEnv "SFA_DBUSER"
  name <- getEnv "SFA_DBNAME"
  pass <- getEnv "SFA_DBPASS"
  return $ "host=" ++ host ++ " port=" ++ port ++ " user=" ++ user ++ " dbname=" ++ name ++ " password=" ++ pass

runAction :: PGInfo -> SqlPersistT (LoggingT IO) a -> IO a
runAction connectionString action =
  runStdoutLoggingT $ filterLogger logFilter $ withPostgresqlConn connectionString $ \backend ->
    runReaderT action backend

runActionR :: SqlPersistT (LoggingT IO) a -> IO a
runActionR action = do
  cStr <- pack <$> rooseliConnString
  runStdoutLoggingT $ filterLogger logFilter $ withPostgresqlConn cStr $ \backend ->
    runReaderT action backend

logFilter :: a -> LogLevel -> Bool
logFilter _ LevelError     = True
logFilter _ LevelWarn      = True
logFilter _ LevelInfo      = True
logFilter _ LevelDebug     = False
logFilter _ (LevelOther _) = True

peelValue :: Sql.Entity record -> record
peelValue (Sql.Entity _ val') = val'

peelValues :: IO [Sql.Entity record] -> IO [record]
peelValues = fmap (fmap (\(Sql.Entity _ val') -> val'))

peelKeyRecord :: Sql.Entity record -> (Key record, record)
peelKeyRecord (Sql.Entity key record) = (key, record)

updateRecord
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => Int64
  -> record
  -> IO Int64
updateRecord recId r = do
  runActionR (Sql.replace (Sql.toSqlKey recId) r)
  pure recId

qRecordById
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => Int64
  -> IO (Maybe record)
qRecordById recId = runActionR (Sql.get $ Sql.toSqlKey recId)

qRecordList
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => IO [Sql.Entity record]
qRecordList = runActionR $ Sql.selectList [] []

qGeoInRegion :: (Sql.ToBackendKey Sql.SqlBackend record)
  => Text
  -> Text
  -> IO [Sql.Entity record]
qGeoInRegion geoTable region = do
  cString <- pack <$> connString "rooseli"
  runAction cString $
    Sql.rawSql [iTrim|
                 SELECT ?? FROM ${geoTable}
                 WHERE ST_Intersects(geom,(
                   SELECT geom
                   FROM osm_admin
                   WHERE name = ?
                 ))
                ;|] [Sql.PersistText region]

qGeoInGeo :: (Sql.ToBackendKey Sql.SqlBackend record)
  => Text
  -> Geo MultiPolygon
  -> IO [Sql.Entity record]
qGeoInGeo geoTable (GeoMultiPolygon mp) = do
  cString <- pack <$> connString "rooseli"
  runAction cString $
    Sql.rawSql [i|select ?? from ${geoTable} where st_contains(geom, ?);|]
    [Sql.PersistLiteralEscaped . writeHex $ MultiPolygonGeometry mp (Just 4326)]

qGeoAtPoint' :: (Sql.ToBackendKey Sql.SqlBackend record)
  => Text
  -> Geo Point
  -> IO [Sql.Entity record]
qGeoAtPoint' geoTable (GeoPoint p) = do
  cString <- pack <$> connString "rooseli"
  runAction cString
    $ Sql.rawSql [i|select ?? from ${geoTable} where st_contains(geom, ?);|]
    [Sql.PersistLiteralEscaped . writeHex $ PointGeometry p (Just 4326)]

qGeoAtPoint :: (Sql.ToBackendKey Sql.SqlBackend record)
  => Text
  -> Geometry Point
  -> IO [Sql.Entity record]
qGeoAtPoint geoTable p = do
  cString <- pack <$> connString "rooseli"
  runAction cString
    $ Sql.rawSql [i|select ?? from ${geoTable} where st_contains(geom, ?);|]
    [Sql.PersistLiteralEscaped . writeHex $ p]

-- | Insert a unique record or not, if it already exists. Either return the key
-- of the new entity, or the key of the conflicting entity.
insertUnique
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => record
  -> IO Int64
insertUnique b = do
  record <- runActionR (Sql.upsert b [])
  return $ Sql.fromSqlKey $ Sql.entityKey record

insertUnique'
  :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  => record
  -> IO record
insertUnique' b = do
  peelValue <$> runActionR (Sql.upsert b [])

delete' :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
  =>
  Key record -> IO ()
delete' = runActionR . Sql.delete

-- delete :: Int64 -> IO ()
-- delete id' = runActionR . Sql.delete $ (Sql.toSqlKey id' :: (PTH.OnlyOneUniqueKey record, Sql.ToBackendKey Sql.SqlBackend record)
--   => Sql.Key record)
