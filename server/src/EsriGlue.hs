{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}


{-|
Admin Ch request need geometry types in Esri syntax. Lets convert our Geo
values to Esri strings!
-}
module EsriGlue where

import qualified Data.ByteString.Lazy        as BL
import           Data.Geometry.Geos.Geometry (Coordinate (..), LinearRing (..),
                                              MultiPolygon (..), Point (..),
                                              Polygon (..))

import           Data.String.Here            (i)
import qualified Data.Vector                 as V

pointToList :: Point -> [Double]
pointToList (Point coord) = peelCoord coord

pgToList :: Polygon -> [[[Double]]]
pgToList pg = V.toList <$> V.toList (dismantlePg pg)

mpToList :: MultiPolygon -> [[[[Double]]]]
mpToList mp = pgToList <$> V.toList (peelMultiPg mp)

pgToEsri' :: Polygon -> BL.ByteString
pgToEsri' = doubleToEsri . peelPg'

peelMultiPg :: MultiPolygon -> V.Vector Polygon
peelMultiPg (MultiPolygon pgV) = pgV

peelPg :: Polygon -> V.Vector (V.Vector Coordinate)
peelPg (Polygon lrV) = coordinateSequenceLinearRing <$> lrV

peelPg' :: Polygon -> V.Vector (V.Vector [Double])
peelPg' (Polygon lrV) = V.singleton singleLr
  -- return the first Linear Ring of the Polygon (without the holes) as
  -- Singleton
  where singleLr = peelCoord <$> coordinateSequenceLinearRing (V.head lrV)

coord2ToDouble :: Coordinate -> (Double,Double)
coord2ToDouble (Coordinate2 x y)   = (x,y)
coord2ToDouble (Coordinate3 x y _) = (x,y)

peelCoord :: Coordinate -> [Double]
peelCoord (Coordinate2 x y)   = [x,y]
peelCoord (Coordinate3 x y _) = [x,y]

dismantlePg :: Polygon -> V.Vector (V.Vector [Double])
dismantlePg pg = fmap peelCoord <$> peelPg pg

doubleToEsri :: V.Vector (V.Vector [Double]) -> BL.ByteString
doubleToEsri pg = [i|{"rings": ${pg}}|]
