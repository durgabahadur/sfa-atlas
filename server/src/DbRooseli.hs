module DbRooseli ( migrate
                 , qEingangByGebaeude
                 , qGebaeudeByEgid
                 ) where

import           Data.ByteString.Char8                (pack)
import           Database.Persist.Class.PersistEntity (Entity (..))
import           Database.Persist.Sql                 (entityDef, getBy,
                                                       selectList, (==.))
import qualified Database.Persist.Sql.Migration       as Mig

import qualified BasicSchema                          as BSch
import qualified Database                             as Db
import qualified SchemaEingang                        as SchE
import qualified SchemaGebaeude                       as SchG
import qualified SchemaParzelle                       as SchP

migrate :: IO ()
migrate = do
  cString <- pack <$> Db.rooseliConnString
  Db.runAction cString $ Mig.runMigration $ Mig.migrate SchE.entityDefs $
    entityDef (Nothing :: Maybe SchE.Eingang)
  Db.runAction cString $ Mig.runMigration SchG.migrateAll
  Db.runAction cString $ Mig.runMigration SchP.migrateAll
  Db.runAction cString $ Mig.runMigration $ Mig.migrate BSch.entityDefs $
    entityDef (Nothing :: Maybe BSch.User)

qGebaeudeByEgid :: Double -> IO (Maybe (Entity SchG.Gebaeude))
qGebaeudeByEgid egid = do
  cString <- pack <$> Db.rooseliConnString
  Db.runAction cString (getBy (SchG.UniqueEgid egid))

-- | Not sure if an Eingang is spatially always inside its Gebaeude polygon.
-- Maybe a portal could also be outside of a gebaeude polygon? To make sure, I
-- added the attribute gebaeudeEgid to the Eingang schema
qEingangByGebaeude :: SchG.Gebaeude -> IO [Entity SchE.Eingang]
qEingangByGebaeude g = do
  cString <- pack <$> Db.rooseliConnString
  Db.runAction cString $
    selectList [SchE.EingangGebaeudeEgid ==. SchG.gebaeudeEgid g] []
