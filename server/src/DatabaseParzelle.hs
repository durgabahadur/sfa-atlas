{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeOperators     #-}

module DatabaseParzelle ( qParzelleByEgrid
                        , qEigentumByEgrid
                        , qEigentumList
                        , qEigentumById
                        ) where

import           Data.Int                        (Int64)
import           Database.Esqueleto.Experimental

import           Data.ByteString.Char8           (pack)
import qualified Data.Text                       as T

import qualified Database                        as Db
import qualified SchemaParzelle                  as SchP

-- shortcut because used heavily
type EigentumL = [( Entity SchP.Eigentum
                  , Maybe (Entity SchP.Besitzende)
                  , Entity SchP.KategorieEigentum
                  )]

eigenQuery :: SqlQuery ( SqlExpr (Entity SchP.Eigentum)
                         :& SqlExpr (Maybe (Entity SchP.Besitzende))
                         :& SqlExpr (Entity SchP.KategorieEigentum)
                          )
eigenQuery = from $ table @SchP.Eigentum
      `leftJoin` table @SchP.Besitzende
      `on` (\(eigen :& besitz) ->
        besitz ?. SchP.BesitzendeId ==. eigen ^. SchP.EigentumBesitzende)
      `innerJoin` table @SchP.KategorieEigentum
      `on` (\(eigen :& _ :& kat) ->
        kat ^. SchP.KategorieEigentumId ==. eigen ^. SchP.EigentumKategorie)

qEigentumList :: IO EigentumL
qEigentumList = Db.runActionR $
                    select $ do
                    (eigen :& besitz :& kat) <- eigenQuery
                    orderBy [ desc (eigen ^. SchP.EigentumDatum) ]
                    pure (eigen, besitz, kat)

qEigentumById :: Int64 -> IO EigentumL
qEigentumById key = Db.runActionR $
    select $ do
    (eigen :& besitz :& kat) <- eigenQuery
    where_ ((eigen ^. SchP.EigentumId) ==. val (toSqlKey key))
    pure (eigen, besitz, kat)


qEigentumByEgrid :: T.Text -> IO [( Entity SchP.Eigentum
                                  , Maybe (Entity SchP.Besitzende)
                                  , Entity SchP.KategorieEigentum
                                  )]
qEigentumByEgrid egrid = do
  cString <- pack <$> Db.rooseliConnString
  Db.runAction cString $
    select $ do
    (eigen :& besitz :& kat) <- eigenQuery
    where_ (eigen ^. SchP.EigentumParzelleEgrid ==. val egrid)
    orderBy [ desc (eigen ^. SchP.EigentumDatum) ]
    pure (eigen, besitz, kat)


qParzelleByEgrid :: T.Text -> IO (Maybe (Entity SchP.Parzelle))
qParzelleByEgrid egrid = do
  cString <- pack <$> Db.rooseliConnString
  Db.runAction cString (getBy $ SchP.UniqueEgrid egrid)
