{-# LANGUAGE DataKinds #-}

module ApiUser where

import qualified ApiGeneric as Generic
import           Servant    (Server)

type API auths = Generic.API Generic.User' auths

server ::Server (API auths)
server = Generic.server

