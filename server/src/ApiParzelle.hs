{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module ApiParzelle where

import           Servant             (Server, err401, (:<|>) (..), (:>))

import qualified ApiGeneric          as Generic
import           BasicSchema         (User)
import           SchemaParzelle      (Besitzende, Eigentum, KategorieEigentum)
import           Servant.Auth.Server (Auth, AuthResult (Authenticated),
                                      throwAll)

type OpenApi auths =
      "parzelle-eigentum"          :> Generic.API Eigentum auths
 :<|> "parzelle-kategorieeigentum" :> Generic.API KategorieEigentum auths

type ProtectedApi auths =
      "parzelle-besitzende"        :> Generic.API Besitzende auths

type API auths = Auth auths User :> (ProtectedApi auths :<|> OpenApi auths)

protected :: AuthResult User -> Server (ProtectedApi auths)
protected (Authenticated _) = Generic.server
protected                 _ = throwAll err401

open :: Server (OpenApi auths)
open = Generic.server :<|> Generic.server

server :: Server (API auths)
server authUsr = protected authUsr :<|> open

