{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}


module RouteParzelle where

import qualified Data.Text           as T
import           Servant             (Capture, Get, Server, (:<|>) (..), (:>))
import           Servant.Auth.Server (Auth, AuthResult (Authenticated),
                                      throwAll)

import           BasicSchema         (User)
import           CommonLucid         (HTML, RawHtml (..))
import           HttpErrors          (unauthorized)
import           ViewGeneric         (AppRoute, ChooseOneRoute, SnippetRoute,
                                      appServer, chooserServer, snippetServer)
import           ViewParzelle        (byEgrid)
import           ViewParzelleModel   (Besitzende, Eigentum, KategorieEigentum)

type EigentumRoute = AppRoute Eigentum
                :<|> SnippetRoute Eigentum

type BesitzendeRoute = AppRoute Besitzende
                   :<|> SnippetRoute Besitzende
                   :<|> ChooseOneRoute Besitzende

type KatEigentumRoute = AppRoute KategorieEigentum
                   :<|> SnippetRoute KategorieEigentum
                   :<|> ChooseOneRoute KategorieEigentum

katEigentumServer :: User -> Server KatEigentumRoute
katEigentumServer usr = appServer usr :<|> snippetServer usr :<|> chooserServer

besitzendeServer :: User -> Server BesitzendeRoute
besitzendeServer usr = appServer usr :<|> snippetServer usr :<|> chooserServer

eigentumServer :: User -> Server EigentumRoute
eigentumServer usr = appServer usr :<|> snippetServer usr

type Route = "parzelle-eigentum"          :> EigentumRoute
        :<|> "parzelle-kategorieeigentum" :> KatEigentumRoute
        :<|> "parzelle-besitzende"        :> BesitzendeRoute
        :<|> "parzelleByEgrid"            :> Capture "egrid" T.Text
                                          :> Get '[HTML] RawHtml

type API auths = Auth auths User :> Route

server :: AuthResult User -> Server Route
server (Authenticated usr) = eigentumServer usr
                        :<|> katEigentumServer usr
                        :<|> besitzendeServer usr
                        :<|> byEgrid (Just usr)
server _ = throwAll unauthorized :<|> throwAll unauthorized
      :<|> throwAll unauthorized :<|> byEgrid Nothing
