{-# LANGUAGE AllowAmbiguousTypes  #-}
{-# LANGUAGE ConstraintKinds      #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeApplications     #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}


module ApiGeneric where

import           Control.Monad              ((>=>))
import           Control.Monad.IO.Class     (liftIO)
import           Control.Monad.Trans.Except (throwE)
import           Data.Int                   (Int64)
import           Servant                    (Capture, Delete, Get, Handler (..),
                                             JSON, NoContent (..), Patch, Post,
                                             ReqBody, Server, err401, errBody,
                                             (:<|>) (..), (:>))
import           Servant.Auth.Server        (Auth, AuthResult (Authenticated),
                                             throwAll)

import           BasicSchema                (User)
import qualified Database                   as Db
import qualified Database.Persist.Sql       as Sql
import qualified Database.Persist.TH        as PTH

class (PTH.OnlyOneUniqueKey a, Sql.ToBackendKey Sql.SqlBackend a)
  => ApiData a

type User' = User
instance ApiData User'

delete :: forall b . ApiData b => Int64 -> Handler NoContent
delete id' =
  let kinfo = Sql.toSqlKey id' :: Sql.Key b
  in (liftIO . Db.runActionR $ Sql.delete kinfo) >> pure NoContent

list :: ApiData a => Handler [a]
list = fmap (\(Sql.Entity _ p) -> p) <$> liftIO Db.qRecordList

update :: ApiData a => Int64 -> a -> Handler Int64
update recId record = liftIO $ Db.updateRecord recId record

create :: ApiData a => a -> Handler Int64
create record = liftIO $ Db.insertUnique record

byId :: ApiData a => Int64 -> Handler a
byId = fetchEntry >=> handleEntry
  where
    err = throwE $ err401 { errBody = "Could not find record with that ID" }
    fetchEntry (usrid :: Int64) = liftIO $ Db.qRecordById usrid
    handleEntry mbEntry =
      case mbEntry of
        Just entry -> return entry
        Nothing    -> Handler err

type Protected a = "create" :> ReqBody '[JSON] a
                            :> Post '[JSON] Int64
              :<|> "update" :> Capture "id" Int64
                            :> ReqBody '[JSON] a
                            :> Patch '[JSON] Int64
              :<|> "delete" :> Capture "id" Int64
                            :> Delete '[JSON] NoContent

type Open a = "list"   :> Get  '[JSON] [a]
         :<|> "get"    :> Capture "id" Int64
                       :> Get  '[JSON] a

type API a auths = Auth auths User' :> (Open a :<|> Protected a)

open :: ApiData a => Server (Open a)
open = list :<|> byId

protected :: forall a . ApiData a => AuthResult User' -> Server (Protected a)
protected (Authenticated _) = create :<|> update :<|> delete @a
protected _                 = throwAll err401

server :: ApiData a => Server (API a auths)
server authUsr = open :<|> protected authUsr
