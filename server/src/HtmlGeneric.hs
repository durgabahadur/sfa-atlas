{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module HtmlGeneric where

import           Data.Maybe (fromMaybe)
import           Data.Text  (Text)
import qualified Data.Text  as T
import           Lucid

chooseFromList :: [(Text,Text,Html ())] -> Html ()
chooseFromList list = if null list
                          then mempty
                          else div_ [ class_ "chooser list" ] $ do
                                 ul_ [] $
                                   foldMap chooseMe list
  where chooseMe (id',name,html) = li_ [ onclick_
                                $ T.concat [ "chooseMe(", id', ",'", name, "')" ]
                                       ] html

openChooserButton :: Text
                  -> Text
                  -> Maybe Text
                  -> Bool
                  -> (Text,Text)
                  -> Text
                  -> Html ()
openChooserButton id' name mbLabel required (valId,valName) chooserName =
  let label = case mbLabel of
                Just l  -> label_ [ for_ id' ] $ toHtml l
                Nothing -> toHtml ("" :: Text)
      req   = if required then "true" else "false"
      val   = if valName == "" then "Bitte wählen" else valName
  in html_ $ do
      label
      button_ [ class_ "value"
              , name_ name
              , data_ "value-type" "number"
              , id_ id'
              , required_ req
              , type_ "button"
              , value_ valId
              , onclick_ (T.concat ["openChooser(this,'", chooserName, "')"])
              ] $ toHtml val
      div_ [ class_ "error-msg" ] ""

listDisplay :: [Html ()] -> Html()
listDisplay list = if null list
                   then mempty
                   else div_ [ class_ "sfadata list"] $ do
                         ul_ [] $
                           foldMap (li_ []) list

cmdButton :: Text -> Text -> Text -> Html ()
cmdButton name cmd icon = button_ [ class_ (T.concat [ name, " icon" ])
                                  , onclick_ cmd
                                  , type_ "button"
                                  ] $ span_ [] $ toHtml icon

editButton :: Text -> Text -> Html ()
editButton id' obj = cmdButton "edit" editMe "✏"
  where editMe = T.concat [ "editMe(", id', ",'", obj, "')" ]

deleteButton :: Text -> Text -> Html ()
deleteButton id' obj = cmdButton "delete" delCmd "🗑"
  where delCmd = T.concat [ "deleteMe(", id', ",'", obj, "')" ]

newButton :: Text -> Html ()
newButton obj = cmdButton "new" newMe "🌟"
  where newMe = T.concat [ "createEmptyForm('", obj, "')" ]

saveButton :: Text -> Text -> Html ()
saveButton id' obj = cmdButton "save" saveMe "💾"
  where saveMe = T.concat [ "saveMe(", id', ",'", obj, "')" ]

withdrawButton :: Text -> Text -> Html ()
withdrawButton id' obj = cmdButton "withdraw" withdrawMe "↩"
  where withdrawMe = T.concat [ "withdrawMe(", id', ",'", obj, "')" ]

itemDisplay :: Text
            -> Maybe Text
            -> Bool
            -> Maybe Text -> Html ()
itemDisplay type' mbLabel localize mbVal =
  let (val, class') = case mbVal of
        Just "" -> ("🕸🕸🕸", "value empty")
        Nothing -> ("🕸🕸🕸", "value empty")
        Just v  -> (v, "value")
      localize' = if localize then "true" else ""
      label = case mbLabel of
        Just l  -> label_ [ for_ l ] $ toHtml l
        Nothing -> toHtml ("" :: Text)
  in html_ $ do
    li_ [ data_ "type" type' ]$ do
      label
      div_ [ class_ class', data_ "localize" localize' ] $ toHtml val

inputNumber :: Text
            -> Text
            -> Maybe Text
            -> Bool
            -> Maybe Text
            -> Html ()
inputNumber id' name mbLabel required mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      label = case mbLabel of
                    Just l  -> label_ [ for_ id' ] $ toHtml l
                    Nothing -> toHtml ("" :: Text)
      req   = if required then "true" else "false"
  in html_ $ do
      label
      input_ [ class_ "value"
              , name_ name
              , type_ "number"
              , data_ "value-type" "number"
              , id_ id'
              , required_ req
              , value_ val
              ]
      div_ [ class_ "error-msg" ] ""

inputDate :: Text
          -> Text
          -> Maybe Text
          -> Bool
          -> Maybe Text
          -> Html ()
inputDate id' name mbLabel required mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      label = case mbLabel of
                    Just l  -> label_ [ for_ id' ] $ toHtml l
                    Nothing -> toHtml ("" :: Text)
      req   = if required then "true" else "false"
      in html_ $ do
          label
          input_ [ class_ "value"
                  , name_ name
                  , type_ "date"
                  , data_ "value-type" "date"
                  , id_ id'
                  , required_ req
                  , value_ val
                  ]
          div_ [ class_ "error-msg" ] ""

sTxtInput :: Bool
          -> Text
          -> Text
          -> Maybe Text
          -> Maybe Text
          -> Html ()
sTxtInput required id' name mbLabel mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      label = case mbLabel of
                    Just l  -> label_ [ for_ id' ] $ toHtml l
                    Nothing -> toHtml ("" :: Text)
      req   = if required then "true" else "false"
      in html_ $ do
          label
          input_ [ class_ "value"
                  , name_ name
                  , data_ "value-type" "text"
                  , id_ id'
                  , required_ req
                  , value_ val
                  ]
          div_ [ class_ "error-msg" ] ""

lTxtInput :: Bool
          -> Text
          -> Text
          -> Maybe Text
          -> Maybe Text
          -> Html ()
lTxtInput required id' name mbLabel mbVal =
  let val   = fromMaybe "🕸🕸🕸" mbVal
      label = case mbLabel of
                    Just l  -> label_ [ for_ id' ] $ toHtml l
                    Nothing -> toHtml ("" :: Text)
      req   = if required then "true" else "false"
      in html_ $ do
          label
          textarea_ [ class_ "value"
                    , name_ name
                    , id_ id'
                    , required_ req
                    ] $ toHtml val
          div_ [ class_ "error-msg" ] ""

itemSearchBox :: Html ()
itemSearchBox = do input_ [ id_ "item-search-input"
                          , placeholder_ "Suchbegriff eingeben"
                          , name_ "search"
                          ]

importItemSearchJs :: Html ()
importItemSearchJs  = script_ [ type_ "text/javascript"
                              , src_ "/static/js/item-search.js"
                              ] ("" :: T.Text)
