{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module ViewGebaeudeModel ( Info (..)
                    , KategorieInfo (..)
                    ) where


import           Data.Aeson   (FromJSON, ToJSON)
import           Data.Int     (Int64)
import qualified Data.Text    as T
import           Data.Time    (UTCTime)
import           GHC.Generics (Generic)

data Info = Info
  { iId       :: Int64
  , iDatum    :: UTCTime
  , iGebEgid  :: Double
  , iText     :: T.Text
  , iAbsender :: Maybe T.Text
  , iKatId    :: Int64
  , iKatName  :: T.Text
  } deriving (Show, Generic, ToJSON, FromJSON)

data KategorieInfo = KategorieInfo
  { kId   :: Int64
  , kName :: T.Text
  } deriving (Show, Generic, ToJSON, FromJSON)

