ipNamespace: { config, lib, pkgs, ... }: with pkgs; {
  environment.systemPackages = with pkgs; [
    firefox
    openssl
    (writeShellScriptBin "setupLxd" ''
      lxc config set core.https_address "${ipNamespace}.88:8443"
      lxc config set core.trust_password "bluff"
      ${pkgs.lxd}/bin/lxc profile edit default < ${pkgs.writeText "profile-default.yaml" ''
        devices:
          eth3:
            name: eth3
            nictype: bridged
            parent: br0
            type: nic
      ''};
    '')
  ];
  networking.defaultGateway = {
    address = "${ipNamespace}.1";
    interface = "eth0";
  };
  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [ 22 222 2222 5432 80 8443 ];
  networking.interfaces.eth0.ipv4.addresses = [{
    address = "${ipNamespace}.88";
    prefixLength = 24;
  }];
  networking.interfaces.br0.ipv4.addresses = [{
    address = "10.0.13.11";
    prefixLength = 24;
  }];
  # 30 1 2022: iptables muss noch angepasst werden, damit nat funzt
  # sudo iptables -t nat -I nixos-nat-post 1 -o br0 -p tcp -j MASQUERADE
  networking.nat = {
    enable = true;
    externalInterface = "eth0";
    internalInterfaces = [ "br0" ];
    forwardPorts = [
      {
        sourcePort = 2222;
        proto = "tcp";
        destination = "10.0.13.93:22";
      }
      {
        sourcePort = 5432;
        proto = "tcp";
        destination = "10.0.13.93:5432";
      }
      {
        sourcePort = 222;
        proto = "tcp";
        destination = "10.0.13.1:22";
      }
      {
        sourcePort = 8000;
        proto = "tcp";
        destination = "10.0.13.1:8000";
      }
      {
        sourcePort = 322;
        proto = "tcp";
        destination = "10.0.13.3:22";
      }
      {
        sourcePort = 443;
        proto = "tcp";
        destination = "10.0.13.3:443";
      }
      {
        sourcePort = 80;
        proto = "tcp";
        destination = "10.0.13.3:80";
      }
    ];
  };
  networking.bridges.br0.interfaces = [];
  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };
  services.xserver = {
    enable = true;

    desktopManager = {
      xterm.enable = false;
    };
    displayManager = {
      defaultSession = "none+i3";
    };
    layout = "us";
    xkbVariant = "intl";
    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [
        dmenu #application launcher most people use
        i3status # gives you the default i3 status bar
        i3lock #default i3 screen locker
        i3blocks #if you are planning on using i3blocks over i3status
      ];
    };
  };
  services.sshd.enable = true;
  # virtualisation.memorySize = 256;

  # gibt neuerdings Fehler bei 'nix build .#lxcHost'
  # muss nach boot per ssh einloggen und dann von Hand machen
  # systemd.services.lxd.postStart =
  # ''
  #   while ! test -S "/var/lib/lxd/unix.socket"; do
  #     echo "Waiting for socket.."
  #     sleep 1
  #   done
  #   echo "...found the socket :)"
  #   lxc config set core.https_address "10.0.1.1:8443"
  #   lxc config set core.trust_password "bluff"
  # '';

  users.users.cri = {
    isNormalUser = true;
    createHome = true;
    home = "/home/cri";
    password = "bling";
    uid = 1000;
    extraGroups = [ "lxd" "wheel" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOf6dgKch+BWz8MbOG6iovPhQQgrx4GRnm78HR3NnayP (none)"
    ];
    shell = pkgs.zsh;
  };
  # virtualisation.graphics = false;
  virtualisation.lxd.enable = true;
  virtualisation.qemu = {
    networkingOptions = [
      "-nic tap,ifname=rooseliTap,script=no,downscript=no,model=virtio-net-pci"
    ];
  };
}
